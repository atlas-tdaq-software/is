#ifndef __IS_IDL__
#define __IS_IDL__

#include "ipc/ipc.idl"

module is
{
    typedef sequence<octet>		data;
    typedef sequence<octet>		address;
    typedef sequence<string>	annotations;
    typedef long long 			time;

    struct type;
    typedef sequence<type> 	types;
    
    struct type
    {
		string		id;
		string		name;
		is::data	code;
        types		nested_types;
    };

    struct tagtime
    {
		long		tag;
        is::time	time;
    };

    struct attributes
    {
		long			tag;
        is::time		time;
        is::address		address;
        is::annotations	ann;
    };

    struct value
    {
        is::attributes	attr;
        is::data		data;
    };
    
    struct info
    {
        string          name;
        is::type        type;
        is::value       value;
    };

    typedef sequence<value>	value_list;
    struct info_history
    {
        string          name;
        is::type        type;
        is::value_list	values;
    };

    struct event
    {
		string		name;
		is::type	type;
        is::tagtime	attr;
    };

    struct criteria
    {
		struct name
		{
		    string	value_;
		    boolean	ignore_;
		} name_;
	
		enum logic { And, Or }	logic_;
	
		struct type
		{
		    ::is::type											value_;
		    enum logic { Ignore, Exact, Not, Base, NotBase }	logic_;
		} type_;
    };

    enum reason { Created, Updated, Deleted, Subscribed };
    enum sorted { ByTag, ByTime };

    typedef sequence<info_history>	info_list;
    typedef sequence<tagtime>		tag_list;

    interface stream
    {
		void push( in info_list il );
		void eof( );
    };
    
    exception	AlreadyExist{};
    exception	InvalidCallback{};
    exception	InvalidCriteria{ string expression; };
    exception	NotCompatible{};
    exception	NotFound{};
    exception	OutOfBounds{};
    exception	SubscriptionNotFound{};

    interface provider
    {
		oneway void command( in string name, in string command );
    };

    interface callback : ipc::servant
    {
    };

    interface event_callback : callback
    {
		oneway void receive( in is::event evt, in is::reason r );
    };

    interface info_callback : callback
    {
		oneway void receive( in is::info inf, in is::reason r );
    };
    
    interface repository : ipc::servant, info_callback
    {
		void add_callback( in string name, in callback cb, in long event_mask )
	        				raises (InvalidCallback);
	        
		boolean contains( in string name );
	        
		void checkin( in ::is::info i, in boolean keep_history, in boolean use_tag )
						raises (NotCompatible);
	
		void create( in ::is::info i )	raises (AlreadyExist);
	
		void create_stream( in is::criteria c, in is::stream s, in long history_depth, in sorted order )
						raises (InvalidCriteria);
	                                        
		void get_last_value( in string name, out ::is::info i )
						raises (NotFound);
	
		void get_tags( in string name, out tag_list tl )
						raises (NotFound); 
	
		void get_type( in string name, out type t )
						raises (NotFound);
	
		void get_value( in string name, in long tag, out ::is::info i )
						raises (NotFound);
	
		void get_last_values( in string name, in long how_many, in sorted order, out ::is::info_history vl )
						raises (NotFound);
	
	
		void remove( in string name )	raises (NotFound);
	
		void remove_all( in is::criteria c ) raises (InvalidCriteria);
	
		void remove_callback( in string name, in callback cb )
						raises (SubscriptionNotFound);
	
		void subscribe( in is::criteria c, in callback cb, in long event_mask )
						raises (InvalidCriteria, InvalidCallback);
	
		void unsubscribe( in is::criteria c, in callback cb )
						raises (SubscriptionNotFound);
	
		void update( in ::is::info i, in boolean keep_history, in boolean use_tag )
						raises (NotFound, NotCompatible);
    };
};

#endif
