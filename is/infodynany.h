#ifndef IS_INFO_DYNANY_H
#define IS_INFO_DYNANY_H

#include <is/infoT.h>
#include <is/infodocument.h>

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

struct ISInfoClonable
{
    virtual ~ISInfoClonable() { ; }
    
    virtual ISInfoClonable * clone() = 0;
    virtual void publishGuts ( ISostream & out ) = 0;
    virtual void refreshGuts ( ISistream & in  ) = 0;
    virtual void print( std::ostream & ) const = 0;
};

/**
 * This class IS info object which can be used for reading and publishing arbitrary information from and to IS
 *
 * @author  Serguei Kolos
 * @version 22/04/05
 */

class ISInfoDynAny : public ISInfo
{
  public:

    /**
     * Default constructor.
     */
    ISInfoDynAny( );

    /**
     * Can be used for publishing arbitrary information.
     * \param partition			IPC partition
     * \param type_name			Name of the IS information type.
     * \throw daq::is::DocumentNotFound	IS meta-information for the given type is not available
     */
    ISInfoDynAny( const IPCPartition & partition, const std::string & type_name );

    /**
     * Creates object of the given type.
     * \param partition		IPC partition
     * \param type		IS information type.
     */
    ISInfoDynAny( const IPCPartition & partition, const ISType & type );

    /**
     * Copy constructor.
     */
    ISInfoDynAny( const ISInfoDynAny & );
    
    /**
     * Copy operator
     */
    ISInfoDynAny & operator=( const ISInfoDynAny & );
    
    /**
     * Checks cardinality of the attribute at the given position.
     * \return 				true if the attribute at the given position is 
     *					multi-value one, otherwise returns false
     * \param pos                       Attribute position. First attribute has position 0.
     */
    bool 
    isAttributeArray( size_t pos ) const;
    
    /**
     * Checks cardinality of the attribute at the given position.
     * \return		true if the given attribute is multi-value one, otherwise returns false
     * \param name	Attribute name.
     */
    bool 
    isAttributeArray( const std::string & name ) const;
    
    /**
     * Returns constant reference to the value of the attribute at the given position.
     * \return		The value of the given attribute
     * \param pos	Attribute position. First attribute has position 0.
     */
    template <typename T>
    const T & 
    getAttributeValue( size_t pos ) const;

    /**
     * Returns reference to the value of the attribute at the given position. 
     * This is lvalue expression which is allowing to modify the attributes value.
     * \return		const reference to the value of the given attribute
     * \param pos	Attribute position. First attribute has position 0.
     */
    template <typename T>
    T & 
    getAttributeValue( size_t pos );

    /**
     * Returns constant reference to the value of the attribute of the given name.
     * \return 					the value of the given attribute
     * \param name				Attribute name.
     * \throw daq::is::DocumentNotFound		IS meta-infomation is not available
     * \throw daq::is::AttributeNotFound	IS type does not contain attribute with the given name
     */
    template <typename T>
    const T & 
    getAttributeValue( const std::string & name ) const;

     /**
     * Returns reference to the value of the attribute of the given name. 
     * This is lvalue expression which allows modifing the attributes value.
     * \return 					const reference to the value of the given attribute
     * \param name				Attribute name.
     * \throw daq::is::DocumentNotFound		IS meta-infomation is not available
     * \throw daq::is::AttributeNotFound	IS type does not contain attribute with the given name
     */
    template <typename T>
    T & 
    getAttributeValue( const std::string & name );

   /**
     * Returns value of the attribute at the given position.
     * \return 					Reference to the object which contains description 
     *						of the given attribute
     * \param pos                       	Attribute position. First attribute has position 0.
     * \throw daq::is::DocumentNotFound		IS meta-infomation is not available
     */
    const ISInfoDocument::Attribute & 
    getAttributeDescription( size_t pos ) const;
    
    /**
     * Returns description of the type of the given attribute.
     * \return 					Reference to the object which contains description 
     *						of the given attribute
     * \param name				Attribute name.
     * \throw daq::is::DocumentNotFound		IS meta-infomation is not available
     * \throw daq::is::AttributeNotFound	IS type does not contain attribute with the given name
     */
    const ISInfoDocument::Attribute & 
    getAttributeDescription( const std::string & name ) const;
    
    /**
     * Returns type id of the attribute at the given position.
     * \return		Enumeration value which identifies the attribute type
     * \param pos	Attribute position. First attribute has position 0.
     */
    ISType::Basic 
    getAttributeType( size_t pos ) const;
        
   /**
     * Returns type id of the attribute with the given name.
     * \return 					Enumeration value which identifies the attribute type
     * \param name				Attribute name.
     * \throw daq::is::DocumentNotFound		IS meta-infomation is not available
     * \throw daq::is::AttributeNotFound	IS type does not contain attribute with the given name
     */
    ISType::Basic 
    getAttributeType( const std::string & name ) const;
    
    /**
     * Returns number of attributes in the object.
     * \return	Number of attributes
     */
    size_t getAttributesNumber() const;
    
    /**
     * Returns description of the object.
     * \return	Reference to the object which contains the IS type description
     */
    const ISInfoDocument & 
    getDescription( ) const;
    

    std::ostream & 
    print( std::ostream & out ) const;
    
    /**
     * Sets value of the attribute at the given position.
     * \param pos	Attribute position. First attribute has position 0.
     * \param value	The new value for the given attribute
     */
    template <typename T>
    void 
    setAttributeValue( size_t pos, const T & value );

    /**
     * Sets value of the attribute with the given name.
     * \param name				Attribute name.
     * \param value 				The new value for the given attribute
     * \throw daq::is::DocumentNotFound		IS meta-infomation is not available
     * \throw daq::is::AttributeNotFound	IS type does not contain attribute with the given name
     */
    template <typename T>
    void 
    setAttributeValue( const std::string & name, const T & value );

  private:
    
    /**
     * Implements deep copy.
     * \param info	Reference to the object which will be copied to this.
     */
    void copyData( const ISInfoDynAny & info );
    
    /**
     * Changes type of the object.
     * \param type	Reference to the object which contains description of IS type
     */
    void setType( const ISType & type );

    void publishGuts( ISostream & out );

    void refreshGuts( ISistream & in );
    
    const ISInfoDocument & assure_document( ) const;

  private:
    std::vector<boost::shared_ptr<ISInfoClonable> >	m_data;
    mutable boost::shared_ptr<ISInfoDocument>		m_doc;
};

template <class T>
struct ISMemberHolder : public ISInfoClonable
{
    ISMemberHolder( )
      : m_value( )
    { ; }

    ISMemberHolder( const T & v )
      : m_value( v )
    { ; }

    ISMemberHolder( T && v )
      : m_value( std::move(v) )
    { ; }

    ISInfoClonable * clone() { return new ISMemberHolder<T>( *this ); }
    void publishGuts ( ISostream & out ) { out << m_value; }
    void refreshGuts ( ISistream & in  ) { in  >> m_value; }

    void setValue ( const T & v ) { m_value = v; }
    const T & getValue ( ) const { return m_value; }
    T & getValue ( ) { return m_value; }

    void print( std::ostream & out ) const {
	is::print( out, m_value );
    }

  private:
    T	m_value;
};

template <typename T>
const T & 
ISInfoDynAny::getAttributeValue( size_t pos ) const
{
    ERS_ASSERT_MSG( pos < m_data.size(), "invalid position " << pos << " is given" );
    const ISMemberHolder<T> * value_holder = dynamic_cast<ISMemberHolder<T>*>( m_data[pos].get() );
    
    ERS_ASSERT_MSG( value_holder, "requested attribute has different type" );
    return value_holder->getValue();
}

template <typename T>
const T & 
ISInfoDynAny::getAttributeValue( const std::string & name ) const 
{
    return getAttributeValue<T>( assure_document( ).attributePosition( name ) );
}

template <typename T>
T & 
ISInfoDynAny::getAttributeValue( size_t pos )
{
    ERS_ASSERT_MSG( pos < m_data.size(), "invalid position " << pos << " is given" );
    ISMemberHolder<T> * value_holder = dynamic_cast<ISMemberHolder<T>*>( m_data[pos].get() );
    
    ERS_ASSERT_MSG( value_holder, "requested attribute has different type" );
    return value_holder->getValue();
}

template <typename T>
T & 
ISInfoDynAny::getAttributeValue( const std::string & name ) 
{
    return getAttributeValue<T>( assure_document( ).attributePosition( name ) );
}

template <typename T>
void
ISInfoDynAny::setAttributeValue( size_t pos, const T & value )
{
    ERS_ASSERT_MSG( pos < m_data.size(), "invalid position " << pos << " is given" );
    ISMemberHolder<T> * value_holder = dynamic_cast<ISMemberHolder<T>*>( m_data[pos].get() );
    
    ERS_ASSERT_MSG( value_holder, "requested attribute has different type" );
    value_holder->setValue( value );
}

template <typename T>
void
ISInfoDynAny::setAttributeValue( const std::string & name, const T & value )
{
    setAttributeValue<T>( assure_document( ).attributePosition( name ), value );
}

inline std::ostream & 
operator<<( std::ostream & out, const ISInfoDynAny & val )
{
    std::vector<int, std::allocator<int>> ii;
    return val.print( out );
}


namespace std {

    struct alloc_wrapper : public ISInfoDynAny {
        using ISInfoDynAny::ISInfoDynAny;
    };

    template<>
    struct allocator<ISInfoDynAny> : public allocator<alloc_wrapper> {
        using allocator<alloc_wrapper>::allocator;
        typedef ISInfoDynAny value_type;

        allocator(const IPCPartition &p, const ISType &t) :
                m_partition(p), m_type(t) {
            ;
        }

        void deallocate(ISInfoDynAny *p, std::size_t ) {
            ::operator delete(p);
        }

        const IPCPartition m_partition;
        const ISType m_type;
    };

    template <> inline
#if (__cplusplus >= 202002L)
    constexpr
#endif
    void vector<ISInfoDynAny>::resize(size_t n) {
        if (n > size()) {
            while(size() != n) {
                push_back(ISInfoDynAny(get_allocator().m_partition, get_allocator().m_type));
            }
        } else {
            while(size() != n) {
                pop_back();
            }
        }
    }
}

#endif
