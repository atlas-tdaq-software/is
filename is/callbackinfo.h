#ifndef IS_CALLBACK_INFO_H
#define IS_CALLBACK_INFO_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      callbackinfo.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISCallbackInfo allows to get pass the information to a 
//				user defined callback function
//////////////////////////////////////////////////////////////////////////////////////
#include <boost/noncopyable.hpp>

#include <is/callbackevent.h>
#include <is/datastream.h>

template <class, class> class ISCallbackImpl;

class ISCallbackInfo :	public ISCallbackEvent
{
    template <class, class> friend class ISCallbackImpl;
    
  public:
    void value ( ISInfo & ) const ;
    
  private:
    ISCallbackInfo(	const IPCPartition & partition,
                        const std::string & server_name,
                        void * user_param,
                        is::reason reason,
                        const is::info & info );
  private:
    mutable is::idatastream     m_data;
    const   is::info &	m_info;
};

#endif
