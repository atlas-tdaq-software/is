#ifndef IS_CALLBACK_TRAITS_H
#define IS_CALLBACK_TRAITS_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      callback.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		implementation of the is::callback_traits
//////////////////////////////////////////////////////////////////////////////////////
#include <boost/function.hpp>
#include <boost/type_traits/remove_const.hpp>
#include <boost/type_traits/remove_reference.hpp>
#include <boost/type_traits/remove_pointer.hpp>
#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>

#include <ipc/object.h>

#include <is/is.hh>

class ISCallbackEvent;
class ISCallbackInfo;
class ISCriteria;
class ISType;

namespace is {
    template<class T, class P = typename boost::remove_const<
            typename boost::remove_pointer<T>::type>::type>
    struct parameter_traits;

    template<class T>
    struct parameter_traits<T, ISCallbackInfo> : public boost::remove_pointer<T> {
        typedef is::info input_type;
        typedef POA_is::info_callback interface_type;
    };

    template<class T>
    struct parameter_traits<T, ISCallbackEvent> : public boost::remove_pointer<T> {
        typedef is::event input_type;
        typedef POA_is::event_callback interface_type;
    };

    struct m__0 {
    };
    struct m__1 {
        m__0 m___t_[2];
    };
    struct m__2 {
        m__1 m___t_[2];
    };
    struct m__3 {
        m__2 m___t_[2];
    };

    template<class T> static m__0 probe(void (T::*)(ISCallbackEvent *));
    template<class T> static m__1 probe(void (T::*)(const ISCallbackEvent *));
    template<class T> static m__2 probe(void (T::*)(ISCallbackInfo *));
    template<class T> static m__3 probe(void (T::*)(const ISCallbackInfo *));

    template<int> struct type_selector;

    template<> struct type_selector<sizeof(m__0)> : public parameter_traits<
            ISCallbackEvent*> {
    };
    template<> struct type_selector<sizeof(m__1)> : public parameter_traits<
            const ISCallbackEvent*> {
    };
    template<> struct type_selector<sizeof(m__2)> : public parameter_traits<
            ISCallbackInfo*> {
    };
    template<> struct type_selector<sizeof(m__3)> : public parameter_traits<
            const ISCallbackInfo*> {
    };

    template<class T>
    struct callback_traits: public type_selector<sizeof(probe(&T::operator()))> {
        typedef void * parameter_type;

        static boost::function<void(typename callback_traits<T>::type*)> make_functor(
                T functor, parameter_type) {
            return functor;
        }
    };

    template<class P>
    struct callback_traits<void (*)(P)> : public parameter_traits<P> {
        typedef void * parameter_type;

        static boost::function<void(P)> make_functor(void (*function)(P),
                parameter_type) {
            return function;
        }
    };

    template<class T, class P>
    struct callback_traits<void (T::*)(P)> : public parameter_traits<P> {
        typedef T * parameter_type;

        static boost::function<void(P)> make_functor(void (T::*function)(P),
                parameter_type self) {
            using namespace boost::placeholders;
            return boost::bind(function, self, boost::placeholders::_1);
        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////
    // This specializations allow to distinguish between 2 subscribe/unsubscribe functions
    // Without them C++ gets confused and is not able to choose the function
    /////////////////////////////////////////////////////////////////////////////////////////
    template<class T> struct callback_traits<T*> ;
    template<> struct callback_traits<ISType> ;
    template<> struct callback_traits<std::string> ;
    template<> struct callback_traits<ISCriteria> ;
}

#endif
