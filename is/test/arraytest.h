#ifndef IS_ARRAY_TEST_H
#define IS_ARRAY_TEST_H

////////////////////////////////////////////////////////////////////////
//
//      is/internal/arraytest.h
//
//	array_test class is used to test the ISInfo functionality
//
//      Sergei Kolos,  November 2004
//	
////////////////////////////////////////////////////////////////////////

#include <is/info.h>

namespace is
{
    const size_t default_array_length = 16;
    
    class array_test : public ISInfo
    {
	char * m_char_array;
	short * m_short_array;
	int * m_int_array;
	int64_t * m_long_array;
	unsigned char * m_uchar_array;
	unsigned short * m_ushort_array;
	unsigned int * m_uint_array;
	uint64_t * m_ulong_array;
	float * m_float_array;
	double * m_double_array;

	char m_c_char_array [default_array_length];
	short m_c_short_array [default_array_length];
	int m_c_int_array [default_array_length];
	int64_t m_c_long_array [default_array_length];
	unsigned char m_c_uchar_array [default_array_length];
	unsigned short m_c_ushort_array [default_array_length];
	unsigned int m_c_uint_array [default_array_length];
	uint64_t m_c_ulong_array [default_array_length];
	float m_c_float_array [default_array_length];
	double m_c_double_array [default_array_length];

	size_t m_array_length;
        
	size_t m_char_length;
	size_t m_short_length;
	size_t m_int_length;
	size_t m_long_length;
	size_t m_uchar_length;
	size_t m_ushort_length;
	size_t m_uint_length;
	size_t m_ulong_length;
	size_t m_float_length;
	size_t m_double_length;
	
	size_t m_c_char_length;
	size_t m_c_short_length;
	size_t m_c_int_length;
	size_t m_c_long_length;
	size_t m_c_uchar_length;
	size_t m_c_ushort_length;
	size_t m_c_uint_length;
	size_t m_c_ulong_length;
	size_t m_c_float_length;
	size_t m_c_double_length;
	
	void initArrays( size_t array_length )
	{
	    m_array_length = array_length;
            
            m_char_array = new char [ ( m_char_length = m_array_length ) * sizeof(char) ];
	    m_short_array = new short [ ( m_short_length = m_array_length ) * sizeof(short) ];
	    m_int_array = new int [ ( m_int_length = m_array_length ) * sizeof(int) ];
	    m_long_array = new int64_t [ ( m_long_length = m_array_length ) * sizeof(int64_t) ];
	    m_uchar_array = new unsigned char [ ( m_uchar_length = m_array_length ) * sizeof(unsigned char) ];
	    m_ushort_array = new unsigned short [ ( m_ushort_length = m_array_length ) * sizeof(unsigned short) ];
	    m_uint_array = new unsigned int [ ( m_uint_length = m_array_length ) * sizeof(unsigned int) ];
	    m_ulong_array = new uint64_t [ ( m_ulong_length = m_array_length ) * sizeof(uint64_t) ];
	    m_float_array = new float [ ( m_float_length = m_array_length ) * sizeof(float) ];
	    m_double_array = new double [ ( m_double_length = m_array_length ) * sizeof(double) ];
	
	    m_c_char_length = m_c_short_length = m_c_int_length = m_c_long_length = m_array_length;
	    m_c_uchar_length = m_c_ushort_length = m_c_uint_length = m_c_ulong_length = m_array_length;
	    m_c_float_length = m_c_double_length = m_array_length;

	    fill( m_char_array );
	    fill( m_short_array );
	    fill( m_int_array );
	    fill( m_long_array );
	    fill( m_uchar_array );
	    fill( m_ushort_array );
	    fill( m_uint_array );
	    fill( m_ulong_array );
	    fill( m_float_array );
	    fill( m_double_array );

	    fill( m_c_char_array );
	    fill( m_c_short_array );
	    fill( m_c_int_array );
	    fill( m_c_long_array );
	    fill( m_c_uchar_array );
	    fill( m_c_ushort_array );
	    fill( m_c_uint_array );
	    fill( m_c_ulong_array );
	    fill( m_c_float_array );
	    fill( m_c_double_array );
	}
    
	void freeArrays()
	{
	    delete[] m_char_array;
	    delete[] m_short_array;
	    delete[] m_int_array;
	    delete[] m_long_array;
	    delete[] m_uchar_array;
	    delete[] m_ushort_array;
	    delete[] m_uint_array;
	    delete[] m_ulong_array;
	    delete[] m_float_array;
	    delete[] m_double_array;
	}
	
	void publishGuts(ISostream& ostrm)
	{
	    ostrm.put( m_char_array, m_array_length );
	    ostrm.put( m_short_array, m_array_length );
	    ostrm.put( m_int_array, m_array_length );
	    ostrm.put( m_long_array, m_array_length );
	    ostrm.put( m_uchar_array, m_array_length );
	    ostrm.put( m_ushort_array, m_array_length );
	    ostrm.put( m_uint_array, m_array_length );
	    ostrm.put( m_ulong_array, m_array_length );
	    ostrm.put( m_float_array, m_array_length );
	    ostrm.put( m_double_array, m_array_length );

	    ostrm.put( m_c_char_array, default_array_length );
	    ostrm.put( m_c_short_array, default_array_length );
	    ostrm.put( m_c_int_array, default_array_length );
	    ostrm.put( m_c_long_array, default_array_length );
	    ostrm.put( m_c_uchar_array, default_array_length );
	    ostrm.put( m_c_ushort_array, default_array_length );
	    ostrm.put( m_c_uint_array, default_array_length );
	    ostrm.put( m_c_ulong_array, default_array_length );
	    ostrm.put( m_c_float_array, default_array_length );
	    ostrm.put( m_c_double_array, default_array_length );
	}

	void refreshGuts(ISistream& istrm)
	{
	    freeArrays(); // dinamic arrays will be allocated by "get" methods
			  // previously allocated memory must be freed
	 
	    istrm.get( &m_char_array, m_char_length );
	    istrm.get( &m_short_array, m_short_length );
	    istrm.get( &m_int_array, m_int_length );
	    istrm.get( &m_long_array, m_long_length );
	    istrm.get( &m_uchar_array, m_uchar_length );
	    istrm.get( &m_ushort_array, m_ushort_length );
	    istrm.get( &m_uint_array, m_uint_length );
	    istrm.get( &m_ulong_array, m_ulong_length );
	    istrm.get( &m_float_array, m_float_length );
	    istrm.get( &m_double_array, m_double_length );

	    istrm.get( m_c_char_array, m_c_char_length);
	    istrm.get( m_c_short_array, m_c_short_length);
	    istrm.get( m_c_int_array, m_c_int_length);
	    istrm.get( m_c_long_array, m_c_long_length);
	    istrm.get( m_c_uchar_array, m_c_uchar_length);
	    istrm.get( m_c_ushort_array, m_c_ushort_length);
	    istrm.get( m_c_uint_array, m_c_uint_length);
	    istrm.get( m_c_ulong_array, m_c_ulong_length);
	    istrm.get( m_c_float_array, m_c_float_length);
	    istrm.get( m_c_double_array, m_c_double_length);
	}
	
    public:
	array_test( size_t array_length = default_array_length )
	{ initArrays( array_length ); }
    
	~array_test()
	{ freeArrays(); }

	void printValues(std::ostream & strm)
	{
	    printAttribute( strm, "array of char \t\t = { ", m_char_array, m_char_length );
	    printAttribute( strm, "array of short \t\t = { ", m_short_array, m_short_length );
	    printAttribute( strm, "array of int \t\t = { ", m_int_array, m_int_length );
	    printAttribute( strm, "array of int64_t \t\t = { ", m_long_array, m_long_length );
	    printAttribute( strm, "array of uchar \t\t = { ", m_uchar_array, m_char_length );
	    printAttribute( strm, "array of ushort \t = { ", m_ushort_array, m_ushort_length );
	    printAttribute( strm, "array of uint \t\t = { ", m_uint_array, m_uint_length );
	    printAttribute( strm, "array of ulong \t\t = { ", m_ulong_array, m_ulong_length );
	    printAttribute( strm, "array of float \t\t = { ", m_float_array, m_float_length );
	    printAttribute( strm, "array of double \t = { ", m_double_array, m_double_length );

	    printAttribute( strm, "const array of char \t\t = { ", m_c_char_array, m_c_char_length );
	    printAttribute( strm, "const array of short \t\t = { ", m_c_short_array, m_c_short_length );
	    printAttribute( strm, "const array of int \t\t = { ", m_c_int_array, m_c_int_length );
	    printAttribute( strm, "const array of int64_t \t\t = { ", m_c_long_array, m_c_long_length );
	    printAttribute( strm, "const array of uchar \t\t = { ", m_c_uchar_array, m_c_char_length );
	    printAttribute( strm, "const array of ushort \t = { ", m_c_ushort_array, m_c_ushort_length );
	    printAttribute( strm, "const array of uint \t\t = { ", m_c_uint_array, m_c_uint_length );
	    printAttribute( strm, "const array of ulong \t\t = { ", m_c_ulong_array, m_c_ulong_length );
	    printAttribute( strm, "const array of float \t\t = { ", m_c_float_array, m_c_float_length );
	    printAttribute( strm, "const array of double \t = { ", m_c_double_array, m_c_double_length );
	}
        
    private:
	template <class T>
	void printAttribute( std::ostream & strm, const char * name, T * array, size_t length )
	{
	    strm << name << "\t\t{ ";
	    for ( size_t i = 0; i < length; i++ )
		strm << array[i] << " ";
	    strm << std::endl;
	}

	template <class T>
	void fill( T * array )
	{
	    for ( size_t i = 0; i < m_array_length; i++ )
		array[i] = (T)3.14 + (T)i;
	}    
    };
}

#endif
