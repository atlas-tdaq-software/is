#ifndef IS_INFO_ENUMERATION_H
#define IS_INFO_ENUMERATION_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infoenumeration.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISInfoEnumeration allows sequential access to multiple IS objects
//////////////////////////////////////////////////////////////////////////////////////
#include <vector>

#include <is/exceptions.h>
#include <is/infostream.h>
#include <is/infodictionary.h>

class ISInfoEnumeration : public ISInfoStream
{
  public:
    ISInfoEnumeration(
            const IPCPartition & partition,
            const std::string & server_name,
            const ISCriteria & criteria );

    operator bool() { return move(0); }
         
    bool operator() ( ) { return move(+1); }
    bool operator++ ( ) { return move(+1); }
    bool operator-- ( ) { return move(-1); }
    bool operator++ ( int ) { return move(+1); }
    bool operator-- ( int ) { return move(-1); }

    unsigned int entries( ) const { return ISInfoStream::entries(); }

    void reset( ) { ISInfoStream::reset(); move(-1); }

    void tags( std::vector< std::pair<int,OWLTime> > & tags ) const;
    
    void value( ISInfo & isi ) const;
                                
    void value( int tag, ISInfo & isi ) const;

    template <class T>
    void values( std::vector<T> & values, int how_many = -1 ) const;

    void sendCommand( const std::string & command ) const;

  private:
    ISInfoDictionary	m_dictionary;
};

template<class T>
void
ISInfoEnumeration::values( std::vector<T> & values, int how_many ) const
{
    T info;    
    if ( !info.m_polymorphic && !info.type().superTypeOf( type() ) )
	throw daq::is::InfoNotCompatible( ERS_HERE, name() );

    m_dictionary.getValues(name(), values, how_many);
}

#endif    // define IS_INFO_ENUMERATION_H
