#ifndef IS_NAMEDINFO_H
#define IS_NAMEDINFO_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      isnamedinfo.h
//
//      public header file for the IS library
//
//      Sergei Kolos Febriary 2000
//
//      description:
//              ISNamedInfo is the base class for IS objects definition
//////////////////////////////////////////////////////////////////////////////////////

#include <is/info.h>
#include <is/infoprovider.h>
#include <is/infodictionary.h>
#include <is/server.h>

class ISNamedInfo : public ISInfo, public ISCommandListener
{
  protected:    

    ISNamedInfo ( const IPCPartition & partition, 
		  const std::string & info_name,
		  const std::string & type_name = ISType::unknown )
      : ISInfo( type_name ),
	m_dict( partition )
    {
	ISInfoProvider::instance().addCommandListener( this );
	name( info_name );
        m_partition = partition;
    }
    
  public:    
    virtual ~ISNamedInfo( )
    { ISInfoProvider::instance().removeCommandListener( this ); }
    
    void checkin( bool keep_history = false ) 
    { m_dict.update( name(), *this, true, keep_history, false ); }
                                
    void checkin( int tag ) 
    { m_dict.update( name(), *this, true, true, true, tag ); }
                                
    void checkout( ) 
    { m_dict.findValue( name(), *this ); }
                                
    void checkout( int tag ) 
    { m_dict.findValue( name(), tag, *this ); }
                                
    void remove( ) const 
    { m_dict.remove( name() ); }
                                        
    bool isExist( ) const 
    { return m_dict.contains( name() ); }
    
    const std::string & name( ) const 
    { return m_name; }
    
    void name( const std::string & name )  {
	if ( name.find( '.' ) == std::string::npos ) {
	    throw daq::is::InvalidName( ERS_HERE, name );
        }
    	m_name = name;
    }
    
    virtual void publishGuts( ISostream & ) = 0;
    virtual void refreshGuts( ISistream & ) = 0;
    
    virtual void command( const std::string & ) { ; }

  private:
    void command( const std::string & name, const std::string & cmd ) {
	if ( name == m_name ) command( cmd );
    }
    
    ISInfoDictionary	m_dict;
    
};

#endif
