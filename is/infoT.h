#ifndef IS_INFOTMPL_H
#define IS_INFOTMPL_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infoT.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//              Class ISInfoT<T> is defined to support 
//              simple types of IS objects. Class T is one of the base C++ types
//////////////////////////////////////////////////////////////////////////////////////

#include <boost/type_traits/integral_promotion.hpp>

#include <is/info.h>

template <class T>
class ISInfoT : public ISInfo
{
    static std::string 
    get_type_name( const T & )
    {
    	return is::type2id<T>::is_array 
        		? "Vector<" + ISType::basicTypeName( is::type2id<T>::id ) + ">"
                        : ISType::basicTypeName( is::type2id<T>::id );
    }
    
public:    
    static const ISType & type() {
	static const ISType m_type = ISInfoT( ).ISInfo::type();
	return m_type;
    }

    inline ISInfoT( ) 
    	: ISInfo( get_type_name( m_value ) ), m_value( ) { ; }
                        
    inline ISInfoT( const T & v ) 
    	: ISInfo( get_type_name( m_value ) ), m_value( v ) { ; }
    
    inline void		setValue ( const T & v ) { m_value = v; }
    inline ISInfoT &	operator= ( const T & v ) { m_value = v; return *this; }
    
    inline const T &	getValue ( ) const { return m_value; }
    inline T &		getValue ( ) { return m_value; }
    inline		operator const T & ( ) const { return m_value; }
    
    inline void		publishGuts ( ISostream & out ) { out << m_value; }
    inline void		refreshGuts ( ISistream & in  ) { in  >> m_value; }
    
    std::ostream &	print( std::ostream & out ) const {
        is::print( out, m_value );
        return out;
    }
    
private:
    T	m_value;
};

template <typename T>
std::ostream & 
operator<<( std::ostream & out, const ISInfoT<T> & val )
{
    return val.print( out );
}

typedef  ISInfoT<bool>			ISInfoBool;
typedef  ISInfoT<char>			ISInfoChar;
typedef  ISInfoT<short>			ISInfoShort;
typedef  ISInfoT<int>			ISInfoInt;
typedef  ISInfoT<int64_t>		ISInfoLong;
typedef  ISInfoT<unsigned char>		ISInfoUnsignedChar;
typedef  ISInfoT<unsigned short>	ISInfoUnsignedShort;
typedef  ISInfoT<unsigned int>		ISInfoUnsignedInt;
typedef  ISInfoT<uint64_t>		ISInfoUnsignedLong;
typedef  ISInfoT<float>			ISInfoFloat;
typedef  ISInfoT<double>		ISInfoDouble;
typedef  ISInfoT<std::string>		ISInfoString;

#endif    // define ISINFOTMPL_H
