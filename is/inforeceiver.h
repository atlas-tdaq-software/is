#ifndef IS_INFO_RECEIVER_H
#define IS_INFO_RECEIVER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      inforeceiver.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISInfoReceiver provides subscriptions for IS repository
//////////////////////////////////////////////////////////////////////////////////////
#include <initializer_list>
#include <functional>
#include <unordered_map>
#include <memory>
#include <vector>

#include <boost/thread/mutex.hpp>

#include <ipc/servantbase.h>

#include <is/is.hh>
#include <is/criteria.h>
#include <is/callbackinfo.h>
#include <is/callbackimpl.h>
#include <is/exceptions.h>

struct ISCallbackHolder;

class ISInfoReceiver
{
    struct Serializer : public IPCObject<POA_is::callback,ipc::single_thread,ipc::transient>
    { };
    
  public:
    explicit ISInfoReceiver( bool serialize_callbacks = false ) 
      : m_serialize_callbacks(serialize_callbacks)
    { ; } 
    
    ISInfoReceiver( const IPCPartition & p, bool serialize_callbacks = false )  
      : m_partition( p ),
	m_serialize_callbacks(serialize_callbacks)
    { ; }
    
    ~ISInfoReceiver( );
    
    const IPCPartition & partition() const
    { return m_partition; }
    
    template <typename F>
    void subscribe (	const std::string & name, 
    			F callback,
			typename is::callback_traits<F>::parameter_type param = 0,
			std::vector<ISInfo::Reason> event_mask =
			    {ISInfo::Created, ISInfo::Updated, ISInfo::Deleted} )
    { subscribe_impl_wrapper( name, make_callback<F>( name, callback, param ), event_mask, false ); }
				
    template <typename F>
    void subscribe (	const std::string & server_name, 
    			const ISCriteria & criteria, 
    			F callback, 
			typename is::callback_traits<F>::parameter_type param = 0,
			std::vector<ISInfo::Reason> event_mask =
			    {ISInfo::Created, ISInfo::Updated, ISInfo::Deleted} )
    { subscribe_impl_wrapper( server_name, criteria, make_callback<F>( server_name, callback, param ), event_mask, false); }
								
    template <typename F>
    void scheduleSubscription (	const std::string & name,
    			F callback,
			typename is::callback_traits<F>::parameter_type param = 0,
			std::vector<ISInfo::Reason> event_mask =
			    {ISInfo::Created, ISInfo::Updated, ISInfo::Deleted} )
    { subscribe_impl_wrapper( name, make_callback<F>( name, callback, param ), event_mask, true ); }

    template <typename F>
    void scheduleSubscription (	const std::string & server_name,
    			const ISCriteria & criteria,
    			F callback,
			typename is::callback_traits<F>::parameter_type param = 0,
			std::vector<ISInfo::Reason> event_mask =
			    {ISInfo::Created, ISInfo::Updated, ISInfo::Deleted} )
    { subscribe_impl_wrapper( server_name, criteria, make_callback<F>( server_name, callback, param ), event_mask, true); }

    void unsubscribe (	const std::string & name, 
    			bool wait_for_completion = false );
    
    void unsubscribe (	const std::string & server_name, 
    			const ISCriteria & criteria,
    			bool wait_for_completion = false );
    
  private:
    /////////////////////////////////
    // Disallow object copying
    /////////////////////////////////
    ISInfoReceiver( const ISInfoReceiver & );
    ISInfoReceiver& operator= ( const ISInfoReceiver & );
    
  private:
    
    template <typename F> 
    IPCServantBase<POA_is::callback> *
    make_callback(	const std::string & name,
			F callback,
                        typename is::callback_traits<F>::parameter_type param )
    {
	std::string::size_type	pos = name.find( '.' );
	std::string server_name( pos == std::string::npos ? name : name.substr( 0, pos ) );
	if (m_serialize_callbacks)
            return new ISCallbackImpl<F, ipc::single_thread>( 
            	m_partition, server_name, callback, param );
        else
	    return new ISCallbackImpl<F, ipc::multi_thread>( 
            	m_partition, server_name, callback, param );
    }
        
    void subscribe_impl_wrapper( const std::string & server_name,
			 const ISCriteria & criteria, 
			 IPCServantBase<POA_is::callback> * cb,
			 const std::vector<ISInfo::Reason> & event_mask,
			 bool no_exception);
                         
    void subscribe_impl_wrapper( const std::string & name,
			 IPCServantBase<POA_is::callback> * cb,
			 const std::vector<ISInfo::Reason> & event_mask,
			 bool no_exception);

    void subscribe_impl( const std::string & key,
			 const std::shared_ptr<ISCallbackHolder> & callback,
			 bool no_exception);

  private:
    typedef std::unordered_map<std::string, std::shared_ptr<ISCallbackHolder>>	CallbackMap;
    
    IPCPartition	m_partition;
    CallbackMap		m_callbacks;
    boost::mutex	m_mutex;
    bool		m_serialize_callbacks;
};

#endif
