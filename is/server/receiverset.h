#ifndef IS_SERVER_RECEIVERSET_H
#define IS_SERVER_RECEIVERSET_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/receiverset.h
//
//      private header file for the IS library
//
//      Sergei Kolos Mai 2013
//
//      description:
//              Class ISReceiverSet defines container for the IS receivers
//////////////////////////////////////////////////////////////////////////////////////

#include <boost/shared_ptr.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include <tbb/queuing_rw_mutex.h>
#include <tbb/spin_rw_mutex.h>

#include <is/server/receiver.h>

using namespace boost::multi_index;

typedef boost::multi_index_container<
	boost::shared_ptr<ISReceiver>,
        indexed_by<
	    hashed_unique<
		const_mem_fun<ISReceiver, const std::string&, &ISReceiver::key>>
        >
> ISReceiverSet;

typedef tbb::queuing_rw_mutex 	ISInfoSetLock;
typedef tbb::spin_rw_mutex 	ISReceiverSetLock;

#endif
