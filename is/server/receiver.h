#ifndef IS_SERVER_RECEIVER_H
#define IS_SERVER_RECEIVER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      receiver.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		declaration of the ISReceiver class
//////////////////////////////////////////////////////////////////////////////////////
#include <list>
#include <string>
#include <unordered_map>
#include <boost/function.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <is/is.hh>
#include <is/server/callback.h>
#include <is/server/criteriahelper.h>
#include <is/server/informer.h>

class ISCallbackHolder;

/**
 * This class implements proxy for remote subscriber
 */
class ISReceiver :  public ISInformer,
		    public boost::enable_shared_from_this<ISReceiver>
{
public:
    ISReceiver(const std::string & server_name, is::callback_ptr receiver);

    ~ISReceiver();

    std::pair<boost::shared_ptr<ISCriteriaHelper>,boost::shared_ptr<ISCallback>>
    addCallback(const is::criteria & criteria, is::callback_ptr r, int event_mask);

    boost::shared_ptr<ISCallback>
    addCallback(const std::string & name, is::callback_ptr r, int event_mask);

    int removeCallback(is::callback_ptr r);

    void applyCallbacksToInfo(ISCallbackHolder & ch) const;

    const std::string & key() const { return ISInformer::getReceiverId(); }
    
    void dump(std::ostream & out);

    void release();

private:
    typedef std::unordered_multimap<
	std::string, boost::shared_ptr<ISCallback>>	Names;
    typedef std::list<
	std::pair<boost::shared_ptr<ISCriteriaHelper>,
	boost::shared_ptr<ISCallback>>>			Criteria;

    mutable boost::mutex m_mutex;
    Names		 m_names;
    Criteria		 m_criteria;
};

#endif
