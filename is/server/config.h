/*
 * config.h
 *
 *  Created on: Jun 12, 2013
 *      Author: kolos
 */

#ifndef IS_SERVER_CONFIG_H_
#define IS_SERVER_CONFIG_H_

#include <stdint.h>

struct ISConfig
{
    static bool		ForcedHistory;
    static int64_t 	HistoryTimeDepth;
    static unsigned int HistoryNumDepth;
    static unsigned int WorkerThreads;
    static unsigned int MaxConcurrentCallbacks;
    static unsigned int MaxCallbacksQueueSize;
    static unsigned int MaxToleratedTimeouts;
    static unsigned int ReceiversCheckupPeriod;
    static unsigned int CallbacksReportThreshold;
};

#endif
