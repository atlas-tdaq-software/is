#ifndef IS_REPOSITORY_H
#define IS_REPOSITORY_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/repository.h
//
//      private header file for the IS server
//
//      Sergei Kolos January 1999
//
//      description:
//              Class ISRepository implements the IS server facility
//////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>

#include <ipc/alarm.h>
#include <ipc/namedobject.h>
#include <ipc/threadpool.h>

#include <is/is.hh>
#include <is/server/infoholder.h>
#include <is/server/infoset.h>
#include <is/server/mirror.h>
#include <is/server/receiver.h>
#include <is/server/receiverset.h>
#include <is/server/streamhelper.h>

class ISRepository: public IPCNamedObject<POA_is::repository>
{
    friend class ISStreamHelper;

    struct DataLock : ISInfoSetLock::scoped_lock
    {
    	DataLock( ISRepository & r, const ISInfoSet *& set )
          : ISInfoSetLock::scoped_lock( r.m_info_mutex, false )
        { set = &r.m_information; }
    };

public:

    void write(std::ostream & out) const;
    void read(std::istream & in);

    ISRepository(const IPCPartition & partition, const std::string & name);

    ~ISRepository();

    ////////////////////////////////////////////////////////////////
    // is.idl methods
    ////////////////////////////////////////////////////////////////
    void add_callback(const char * name, is::callback_ptr cb, CORBA::Long event_mask);
    void checkin(const is::info & inf, bool keep_history, bool use_tag);
    void create(const is::info & inf);
    void create_stream(const is::criteria & criteria, is::stream_ptr stream,
	    CORBA::Long history_depth, is::sorted order);
    bool contains(const char * name);
    void get_last_value(const char * name, is::info_out data);
    void get_last_values(const char * name, CORBA::Long how_many, is::sorted order, is::info_history_out ih);
    void get_tags(const char * name, is::tag_list_out tags);
    void get_type(const char * name, is::type_out type);
    void get_value(const char * name, CORBA::Long tag, is::info_out data);
    void receive(const is::info & info, is::reason reason);
    void remove(const char * name);
    void remove_all(const is::criteria & criteria);
    void remove_callback(const char * name, is::callback_ptr cb);
    void subscribe(const is::criteria & criteria, is::callback_ptr cb, CORBA::Long event_mask);
    void unsubscribe(const is::criteria & criteria, is::callback_ptr cb);
    void update(const is::info & inf, bool keep_history, bool use_tag);

    void shutdown();
    void test(const char * name);
    void clearDeadReceivers();

    static std::string constructReceiverID(is::callback_ptr r);

private:
    boost::shared_ptr<ISInfoHolder> find(const char * name);

    mutable ISReceiverSetLock 	m_receiver_mutex;
    mutable ISInfoSetLock 	m_info_mutex;

    ISMirror			m_mirror;	// The order is important
    ISInfoSet 			m_information;	// when IS server stops the mirror server will get Deleted notifications for all objects
    ISReceiverSet		m_receivers;	// Normal subscribers will not get such notifications
    IPCThreadPool		m_stream_pool;
};

#endif    // define IS_REPOSITORY_H
