#ifndef IS_SERVER_BACKUP_H
#define IS_SERVER_BACKUP_H

#include <ers/ers.h>

/*! \class is::BadBackupFile
 */
 ERS_DECLARE_ISSUE(	is,
 			BadBackupFile, 
                        "IS backup file is invalid or corrupted : " << reason,
                        ((std::string)reason)
                  )

/*! \class is::CantReadFile
 */
 ERS_DECLARE_ISSUE(	is,
 			CantReadFile, 
                        "Can not read from the \"" << name << "\" file.",
                        ((std::string)name)
                  )

/*! \class is::CantOpenFile
 */
 ERS_DECLARE_ISSUE(	is,
 			CantOpenFile, 
                        "Can not open the \"" << name << "\" file.",
                        ((std::string)name)
                  )

/*! \class is::CantWriteFile
 */
 ERS_DECLARE_ISSUE(	is,
 			CantWriteFile, 
                        "Can not write the \"" << name << "\" file.",
                        ((std::string)name)
                  )

#endif
