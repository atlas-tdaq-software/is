#ifndef IS_CALLBACK_HOLDER_H
#define IS_CALLBACK_HOLDER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/internal/callbackholder.h
//
//      private header file for the IS library
//
//      Sergei Kolos February 2014
//
//      description:
//              Class ISCallbackHolder provides storage for IS object callbacks
//
//////////////////////////////////////////////////////////////////////////////////////

#include <list>
#include <string>
#include <thread>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <is/is.hh>
#include <is/server/mirror.h>
#include <is/server/callback.h>
#include <is/server/receiverset.h>

class ISCallbackHolder : boost::noncopyable
{
  public:

    ISCallbackHolder( const is::info & info, const ISReceiverSet & receivers, 
    	ISReceiverSetLock & receivers_mutex, ISMirror & m );
        
    ISCallbackHolder( const std::string & name, const is::type & type, ISMirror & m );

    virtual ~ISCallbackHolder( ) { ; }

    virtual void addCallback( const boost::shared_ptr<ISCallback> & r );

    void maybeAddCallback( const ISCriteriaHelper & c, const boost::shared_ptr<ISCallback> & r );

    const std::string & stdname() const
    { return m_name; }

  protected:
    void inform( const is::info & info, is::reason reason );

  protected:
    const std::string	m_name;
    const is::type	m_type;

  private:
    typedef std::list<boost::weak_ptr<ISCallback>> Callbacks;
    
    std::mutex		m_callbacks_mutex;
    Callbacks		m_callbacks;
    ISMirror &		m_mirror;
};

#endif
