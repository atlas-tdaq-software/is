#ifndef IS_INFOPROVIDER_H
#define IS_INFOPROVIDER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infoprovider.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISCommandListener is the base class for the user defined info providers
//////////////////////////////////////////////////////////////////////////////////////

#include <list>

#include <boost/thread.hpp>

#include <ipc/object.h>

#include <is/is.hh>

#ifdef __rtems__
namespace boost
{
    typedef mutex recursive_mutex;
}
#endif

class ISInfoDictionary;
class ISInfo;

class ISCommandListener
{
  public:
    virtual void command( const std::string & name, const std::string & cmd ) = 0;
    virtual ~ISCommandListener()
    { ; }
};

class ISInfoProvider : public IPCObject<POA_is::provider,ipc::multi_thread,ipc::persistent>
{
    friend class ISInfo;
    friend class ISInfoDictionary;
    template <class T> friend class ISProxy;
    friend void operator<<= ( is::info & , ISInfo & );
  
  public:
    static ISInfoProvider & instance();
    
    void addCommandListener	( ISCommandListener * lst );
    void removeCommandListener	( ISCommandListener * lst );
    
    std::string unique_id() const;
    
  private:
    ISInfoProvider();
    
    void command ( const char * name, const char * command );
    
    const is::address & address() { return m_address; }

    static std::string makeConnectString( const is::address & addr )
    { 
    	return ( m_corbaloc_prefix + std::string( (const char *)addr.get_buffer(), addr.length() ) ); 
    }
        
    static void	atExitFun();
    
    typedef std::list<ISCommandListener *> Listeners;
    
    boost::recursive_mutex	m_mutex;
    is::address			m_address;
    Listeners			m_listeners;
    
    static boost::mutex m_guard;
    static ISInfoProvider * m_provider;
    static std::string m_corbaloc_prefix;
};

#endif
