#! /bin/sh
#
#--------------------------------------------------------------------
# 
# The is_editor.sh script is used to run OKS Schema Editor
# with the IS repository file.
# 
# Use 'is_editor.sh --help' to learn more about script options.
# 
#--------------------------------------------------------------------
#
# Written 20/08/99 by Serguei Kolos <Serguei.Kolos@cern.ch>
#
# Modified:
#	-
#
#--------------------------------------------------------------------

params=$@

unset TDAQ_DB_REPOSITORY

if [ "$1" = "-h" ]
then
      echo 'Usage: is_repository_editor.sh [file...] [-h]'
      echo 'Arguments/Options:'
      echo '	file...		specifies IS schema file(s) which will be loaded'
      echo '	-h		print this help information'
      echo ''
      echo 'Description:'
      echo '     Runs oks_schema_editor with the IS repository files.'
      exit 0
fi

data_file=`echo $0 | sed -e 's/\(.*\/\).*\/bin\/is_edit_repository.sh/\1\share\/data\/is\/is\.xml/g'`

oks_schema_editor ${data_file} $@

################################################################################
