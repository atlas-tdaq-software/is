////////////////////////////////////////////////////////////////////////////
//      proxy.cc
//
//      Implementation of the IS proxy library
//
//      Serguei Kolos August 2007
//
//      description:
//              This file contains implementation of the IS service proxy
//
////////////////////////////////////////////////////////////////////////////

#include <is/is.hh>
#include <ipc/proxy/factory.h>

namespace
{
  IPCProxyFactory<is::repository, POA_is::repository, POA_is::repository_tie> __f1__;
}
