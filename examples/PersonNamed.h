#ifndef PERSONNAMED_H
#define PERSONNAMED_H

#include <is/namedinfo.h>

#include "Address.h"
#include "Child.h"
#include <string>
#include <ostream>
#include <vector>
#include <owl/time.h>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Describes a human person
 * 
 * @author  produced by the IS generator
 */

class PersonNamed : public ISNamedInfo {
public:
    enum Sex {male,female};


    /**
     * Person's name
     */
    std::string                   name;

    /**
     * Person's date of birth
     */
    OWLDate                       birth_date;

    /**
     * Person's sex
     */
    Sex                           sex;

    /**
     * Person's home address
     */
    Address                       address;

    /**
     * Person's children
     */
    std::vector<Child>            children;


    static const ISType & type() {
	static const ISType type_ = PersonNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "name: " << name << "\t// Person's name" << std::endl;
	out << "birth_date: " << birth_date << "\t// Person's date of birth" << std::endl;
	out << "sex: " << sex << "\t// Person's sex" << std::endl;
	out << "address: " << address << "\t// Person's home address" << std::endl;
	out << "children[" << children.size() << "]:\t// Person's children" << std::endl;
	for ( size_t i = 0; i < children.size(); ++i )
	    out << i << " : " << children[i] << std::endl;
	return out;
    }

    PersonNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "Person" )
    {
	initialize();
    }

    ~PersonNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    PersonNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << name << birth_date << sex << address << children;
    }

    void refreshGuts( ISistream & in ){
	in >> name >> birth_date >> sex >> address >> children;
    }

private:
    void initialize()
    {
	this -> name = "Jone";
	sex = male;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const PersonNamed & info ) {
    info.print( out );
    return out;
}

#endif // PERSONNAMED_H
