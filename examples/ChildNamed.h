#ifndef CHILDNAMED_H
#define CHILDNAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>
#include <owl/time.h>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Describes a human's child
 * 
 * @author  produced by the IS generator
 */

class ChildNamed : public ISNamedInfo {
public:
    enum Sex {male,female};


    /**
     * Person's name
     */
    std::string                   name;

    /**
     * Person's date of birth
     */
    OWLDate                       birth_date;

    /**
     * Person's sex
     */
    Sex                           sex;


    static const ISType & type() {
	static const ISType type_ = ChildNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "name: " << name << "\t// Person's name" << std::endl;
	out << "birth_date: " << birth_date << "\t// Person's date of birth" << std::endl;
	out << "sex: " << sex << "\t// Person's sex";
	return out;
    }

    ChildNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "Child" )
    {
	initialize();
    }

    ~ChildNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ChildNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << name << birth_date << sex;
    }

    void refreshGuts( ISistream & in ){
	in >> name >> birth_date >> sex;
    }

private:
    void initialize()
    {
	this -> name = "Jone";
	sex = male;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ChildNamed & info ) {
    info.print( out );
    return out;
}

#endif // CHILDNAMED_H
