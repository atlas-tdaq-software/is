SHELL = /bin/sh

#########################################################
#	Backend software root
#########################################################

RELEASE_DIR 	= /afs/cern.ch/atlas/project/tdaq/cmt/current/installed

#########################################################
#	Compiler 
#########################################################

#Uncomment this for Solaris
#JAVA 		= /afs/cern.ch/sw/java/sparc_solaris26/jdk/sun-1.4.1/bin/java
#JAVAC 		= /afs/cern.ch/sw/java/sparc_solaris26/jdk/sun-1.4.1/bin/javac

#Uncomment this for Linux
JAVA 		= /afs/cern.ch/sw/java/i386_redhat73/jdk/sun-1.4.1/bin/java
JAVAC 		= /afs/cern.ch/sw/java/i386_redhat73/jdk/sun-1.4.1/bin/javac



#########################################################
#	main target
#########################################################

all: ReadInfo.java PersonNamed.java EmployeeNamed.java
	${JAVAC} -classpath  .:${RELEASE_DIR}/share/lib/ipc.jar:${RELEASE_DIR}/share/lib/is.jar:${RELEASE_DIR}/share/lib/rdb.jar \
			       *.java

#########################################################
#	generation target
#########################################################
PersonNamed.java EmployeeNamed.java : ../data/is_example.xml
	is_generator.sh -java -n ../data/is_example.xml

#########################################################
#	run target
#########################################################
run: *.class
	${JAVA} -classpath .:${RELEASE_DIR}/share/lib/ipc.jar:${RELEASE_DIR}/share/lib/is.jar:${RELEASE_DIR}/share/lib/rdb.jar \
			     ReadInfo IStest Person1 Employee1 MyPartition

#########################################################
#	clean target
#########################################################

clean:
	rm -f *.class	
