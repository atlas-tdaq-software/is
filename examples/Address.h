#ifndef ADDRESS_H
#define ADDRESS_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

class Address : public ISInfo {
public:

    /**
     */
    unsigned short                number;

    /**
     */
    std::string                   street;

    /**
     */
    std::string                   town;

    /**
     */
    unsigned int                  zip;


    static const ISType & type() {
	static const ISType type_ = Address( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "number: " << number << "\t// " << std::endl;
	out << "street: " << street << "\t// " << std::endl;
	out << "town: " << town << "\t// " << std::endl;
	out << "zip: " << zip << "\t// ";
	return out;
    }

    Address( )
      : ISInfo( "Address" )
    {
	initialize();
    }

    ~Address(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Address( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << number << street << town << zip;
    }

    void refreshGuts( ISistream & in ){
	in >> number >> street >> town >> zip;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Address & info ) {
    info.print( out );
    return out;
}

#endif // ADDRESS_H
