#ifndef ADDRESSNAMED_H
#define ADDRESSNAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

class AddressNamed : public ISNamedInfo {
public:

    /**
     */
    unsigned short                number;

    /**
     */
    std::string                   street;

    /**
     */
    std::string                   town;

    /**
     */
    unsigned int                  zip;


    static const ISType & type() {
	static const ISType type_ = AddressNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "number: " << number << "\t// " << std::endl;
	out << "street: " << street << "\t// " << std::endl;
	out << "town: " << town << "\t// " << std::endl;
	out << "zip: " << zip << "\t// ";
	return out;
    }

    AddressNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "Address" )
    {
	initialize();
    }

    ~AddressNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    AddressNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << number << street << town << zip;
    }

    void refreshGuts( ISistream & in ){
	in >> number >> street >> town >> zip;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const AddressNamed & info ) {
    info.print( out );
    return out;
}

#endif // ADDRESSNAMED_H
