package is;

public class Annotation {
    private String data;

    @FunctionalInterface
    public interface Parser<T> {
        public T parse(String s);
    }

    public <T> Annotation(T value) {
        this.data = value.toString();
    }
    
    public <T> void setValue(T value) {
        this.data = value.toString();
    }
    
    public <T> T getValue(Parser<T> parser) {
        return parser.parse(data);
    }
    
    public String data() {
        return data;
    }
}
