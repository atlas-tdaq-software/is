package is;

/**
 * Thrown to indicate that the IS information object
 * already exist in the IS repository
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class InfoAlreadyExistException extends Exception 
{
    /**
     * Constructs an InfoAlreadyExistException with the specified
     * information name.
     *
     * @param name the information name
     */
    public InfoAlreadyExistException( String name ) 
    {
	super( "Information '" + name + "' already exists" );
    }
}
