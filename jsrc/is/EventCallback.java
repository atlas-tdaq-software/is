package is;

import ipc.Partition;

class EventCallback extends CallbackHolder<is.event_callback> 
		implements event_callbackOperations
{
    final Partition	partition;
    final String	server;

    EventCallback( Partition partition, String server, Listener listener )
    {
        super(listener);
	this.partition = partition;
        this.server = server;
    }
    
    public final void receive( is.event event, is.reason r )
    {    
	event.name = server + '.' + event.name;
	receive(new InfoEvent( partition, event ), r);
    }
}
