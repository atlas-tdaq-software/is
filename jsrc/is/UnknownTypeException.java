package is;

/**
 * Thrown to indicate that description for the IS information type 
 * is not available.
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class UnknownTypeException extends Exception 
{
    /**
     * Constructs an UnknownTypeException with no detail message.
     */
    public UnknownTypeException( Exception ex )
    {
	super("Can't get IS information type description", ex);
    }

    /**
     * Constructs an UnknownTypeException with the specified
     * detail message.
     *
     * @param message the detail message
     */
    public UnknownTypeException( String message ) 
    {
	super(message);
    }
}
