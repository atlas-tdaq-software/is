package is;

/**
 * Thrown to indicate that the IS information object
 * does not exist in the IS repository
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class InfoNotFoundException extends Exception 
{
    /**
     * Constructs an InfoNotFoundException with the specified
     * information name.
     *
     * @param name the information name
     */
    public InfoNotFoundException( String name ) 
    {
	super( "Information '" + name + "' does not exist" );
    }
}
