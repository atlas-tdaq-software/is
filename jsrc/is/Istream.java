package is;

/**
 * Istream provides interface for unmarshalling attributes of the 
 * user-defined information class from the IS stream.
 * @author Sergei Kolos
 */
public interface Istream
{
    /**
     * Reads a single enumeration value from this stream.
     * @return enumeration value read from this stream
     */
    public <E extends Enum<E>> E getEnum( Class<E> cl );
    
    /**
     * Reads a single boolean value from this stream.
     * @return boolean value read from this stream
     */
    public boolean getBoolean();
    
    /**
     * Reads a single byte value from this stream.
     * @return byte value read from this stream
     */
    public byte getByte();
    
    /**
     * Reads a single short value from this stream.
     * @return short value read from this stream
     */
    public short getShort();
    
    /**
     * Reads a single int value from this stream.
     * @return int value read from this stream
     */
    public int getInt( );
    
    /**
     * Reads a single long value from this stream.
     * @return int value read from this stream
     */
    public long getLong( );

    /**
     * Reads a single float value from this stream.
     * @return float value read from this stream
     */
    public float getFloat();
    
    /**
     * Reads a single double value from this stream.
     * @return double value read from this stream
     */
    public double getDouble();
    
    /**
     * Reads a single string value from this stream.
     * @return String value read from this stream
     */
    public String getString();
    
    /**
     * Reads a single date value from this stream.
     * @return Date value read from this stream
     */
    public Date getDate();

    /**
     * Reads a single time value from this stream.
     * @return Time value read from this stream
     */
    public Time getTime();

    /**
     * Reads a single Information object from this stream.
     * @param cl Class of the Information object
     * @return Information object read from this stream
     */
    public <T extends Info> T getInfo( Class<T> cl );

    /**
     * Reads an array of enumeration values from this stream.
     * @return array of enumeration values from this stream
     */
    public <E extends Enum<E>> E[] getEnumArray( Class<E> cl );
    
    /**
     * Reads an array of boolean values from this stream.
     * @return array of booleans read from this stream
     */
    public boolean[] getBooleanArray( );
    
    /**
     * Reads an array of byte values from this stream.
     * @return array of bytes read from this stream
     */
    public byte[] getByteArray( );
    
    /**
     * Reads an array of short values from this stream.
     * @return array of shorts read from this stream
     */
    public short[] getShortArray( );
    
    /**
     * Reads an array of int values from this stream.
     * @return array of ints read from this stream
     */
    public int[] getIntArray( );
    
    /**
     * Reads an array of long values from this stream.
     * @return array of ints read from this stream
     */
    public long[] getLongArray( );

    /**
     * Reads an array of float values from this stream.
     * @return array of floats read from this stream
     */
    public float[] getFloatArray( );
    
    /**
     * Reads an array of double values from this stream.
     * @return array of doubles read from this stream
     */
    public double[] getDoubleArray( );
    
    /**
     * Reads an array of string values from this stream.
     * @return array of Strings read from this stream
     */
    public String[] getStringArray( );
    
    /**
     * Reads an array of date values from this stream.
     * @return array of Dates read from this stream
     */
    public Date[] getDateArray( );
    
    /**
     * Reads an array of time values from this stream.
     * @return array of Times read from this stream
     */
    public Time[] getTimeArray( );
    
    /**
     * Reads an array of Information objects from this stream.
     * @param cl Class of the Information object
     * @return array of Times read from this stream
     */
    public <T extends Info> T[] getInfoArray( Class<T> cl );
        
    /**
     * Reads a vector of Information objects from this stream.
     * @param cl Class of the Information object
     * @return vecotr of Info objects read from this stream
     */
    public <T extends Info> java.util.Vector<T> getInfoVector( Class<T> cl );
}
