package is;

/**
 * The listener interface for receiving commands. The class that is interested in receiving commands has to implement this interface.
 * The listener object created from that class is then registered using the addCommandListenermethod of the InfoProvider class.
 * @author Sergei Kolos
 * @see InfoProvider
 */

public interface CommandListener
{
    /**
     * Invoked when the command was sent to this provider.
     * @param name name of the information object to which this command applies
     * @param cmd command
     */
    void command( String name, String cmd );    
}

