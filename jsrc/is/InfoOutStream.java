package is;

import java.lang.*;
import java.util.Hashtable;
import java.util.zip.Deflater;
import org.jacorb.orb.CDROutputStream;

/**
 * InfoOutStream implements the Ostream interface.
 * @author Sergei Kolos
 */
class InfoOutStream implements Ostream
{
    private static final int	MINIMUM_SIZE_FOR_COMPRESSION = 10000;
    private CDROutputStream	out;
    private int			header_size;
    
    InfoOutStream( )
    {
	out = new CDROutputStream();
	out.write_boolean( false );	// byte order is always big-endian	
	out.write_boolean( false );	// uncompressed by default
	out.write_ulong( 0 );		// reserve space for the uncompressed data size
    	header_size = out.size();
    }
    
    byte[] getData( )
    { 
	byte[] data = out.getBufferCopy();
        
        if ( data.length < MINIMUM_SIZE_FOR_COMPRESSION )
        {
	    return data;
        }
        
        out = new CDROutputStream();
        out.write_boolean( false );				// byte order is always big-endian	
	out.write_boolean( true );				// compressed
	out.write_ulong( data.length - header_size );		// store uncompressed data size
	Deflater zout = new Deflater( 1 );			// lowest, but fastest compression level
	zout.setInput( data, header_size, data.length - header_size );
	zout.finish();

	int maximum_compressed_size = (( data.length - header_size + 12 ) * 1001)/1000 + 1 + header_size;
	byte[] buffer = new byte[maximum_compressed_size];
	int compressed_size = zout.deflate( buffer, header_size, maximum_compressed_size - header_size );

	int full_size = compressed_size + header_size;
	byte[] compressed_data = new byte[full_size];
	byte[] header = out.getBufferCopy();
	for ( int i = 0; i < header_size; i++ )
	{
	    compressed_data[i] = header[i];
	}
	for ( int i = header_size; i < full_size; i++ )
	{
	    compressed_data[i] = buffer[i];
	}
	return compressed_data;
    }
    
    /**
     * Writes a single integer value to this stream.
     * @param v the enumeration value to be written
     * @return this stream object
     */
    public <E extends Enum<E>> Ostream put( java.lang.Enum<E> v )
    {
	out.write_long( v.ordinal() );
	return this;
    }
    
    /**
     * Writes a single boolean value to this stream.
     * @param v the boolean to be written
     * @return this stream object
     */
    public Ostream put( boolean v )
    {
	out.write_boolean( v );
	return this;
    }
    /**
     * Writes a single byte value to this stream.
     * @param v the byte to be written
     * @param sign if true the v will be interpreted as signed byte; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( byte v, boolean sign )
    {
	if ( sign )
	    out.write_char( (char)v );
	else
	    out.write_octet( v );
	return this;
    }
    
    /**
     * Writes a single short value to this stream.
     * @param v the short to be written
     * @param sign if true the v will be interpreted as signed short; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( short v, boolean sign )
    {
	if ( sign )
	    out.write_short( v );
	else
	    out.write_ushort( v );
	return this;
    }
    
    /**
     * Writes a single integer value to this stream.
     * @param v the int to be written
     * @param sign if true the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( int v, boolean sign )
    {
	if ( sign )
	    out.write_long( v );
	else
	    out.write_ulong( v );
	return this;
    }

    /**
     * Writes a single long value to this stream.
     * @param v the int to be written
     * @param sign if true the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( long v, boolean sign )
    {
        if ( sign )
            out.write_longlong( v );
        else
            out.write_ulonglong( v );
        return this;
    }
    
    /**
     * Writes a single float value to this stream.
     * @param v the float to be written
     * @return this stream object
     */
    public Ostream put( float v )
    {
	out.write_float( v );
	return this;
    }
    
    /**
     * Writes a single double value to this stream.
     * @param v the double to be written
     * @return this stream object
     */
    public Ostream put( double v )
    {
	out.write_double( v );
	return this;
    }

    /**
     * Writes a single string value to this stream.
     * @param v the String to be written
     * @return this stream object
     */
    public Ostream put( String v )
    {
	out.write_string( v );
	return this;
    }    

    /**
     * Writes a single date value to this stream.
     * @param v the Date to be written
     * @return this stream object
     */
    public Ostream put( Date v )
    {
	out.write_ulong( v == null ? 0 : (int)(v.getTime()/1000l) );
	return this;
    }
    
    /**
     * Writes a single time value to this stream.
     * @param v the Time to be written
     * @return this stream object
     */
    public Ostream put( Time v )
    {
	out.write_longlong( v == null ? 0 : v.getTimeMicro() );
	return this;
    }    
    
    /**
     * Writes a single Infomation object value to this stream.
     * @param v the Information object to be written
     * @param type the IS type of Infomation object
     * @return this stream object
     */
    public Ostream put( Info v, is.Type type )
    {
	v.publishGuts( this );
	return this;
    }        
    
    /**
     * Writes an array of boolean values to this stream.
     * @param array the array of enumeration values to be written
     * @return this stream object
     */
    public <E extends Enum<E>> Ostream put( java.lang.Enum<E>[] v )
    {
	out.write_ulong( v.length );
	for( int i = 0; i < v.length; i++ )
	    out.write_long( v[i].ordinal() );
	return this;
    }
    
    /**
     * Writes an array of boolean values to this stream.
     * @param v the array of booleans to be written
     * @return this stream object
     */
    public Ostream put( boolean[] v )
    {
	out.write_ulong( v.length );
	out.write_boolean_array( v, 0, v.length );
	return this;
    }
    
    /**
     * Writes an array of byte values to this stream.
     * @param v the array of bytes to be written
     * @param sign if true any alement of the v will be interpreted as signed byte; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( byte[] v, boolean sign )
    {
	out.write_ulong( v.length );
	if ( sign )
	{
	    for( int i = 0; i < v.length; i++ )
		out.write_char( (char)v[i] );
	}
	else
	    out.write_octet_array( v, 0, v.length );
	return this;
    }
	
    /**
     * Writes an array of short values to this stream.
     * @param v the array of shorts to be written
     * @param sign if true any alement of the v will be interpreted as signed short; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( short[] v, boolean sign )
    {
	out.write_ulong( v.length );
	if ( sign )
	    out.write_short_array( v, 0, v.length );
	else
	    out.write_ushort_array( v, 0, v.length );
	return this;
    }
    
    /**
     * Writes an array of int values to this stream.
     * @param v the array of ints to be written
     * @param sign if true any alement of the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( int[] v, boolean sign )
    {
	out.write_ulong( v.length );
	if ( sign )
	    out.write_long_array( v, 0, v.length );
	else
	    out.write_ulong_array( v, 0, v.length );
	return this;
    }

    /**
     * Writes an array of long values to this stream.
     * @param v the array of ints to be written
     * @param sign if true any alement of the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( long[] v, boolean sign )
    {
        out.write_ulong( v.length );
        if ( sign )
            out.write_longlong_array( v, 0, v.length );
        else
            out.write_ulonglong_array( v, 0, v.length );
        return this;
    }

    /**
     * Writes an array of float values to this stream.
     * @param v the array of floats to be written
     * @return this stream object
     */
    public Ostream put( float[] v )
    {
	out.write_ulong( v.length );
	out.write_float_array( v, 0, v.length );
	return this;
    }
    
    /**
     * Writes an array of double values to this stream.
     * @param v the array of doubles to be written
     * @return this stream object
     */
    public Ostream put( double[] v )
    {
	out.write_ulong( v.length );
	out.write_double_array( v, 0, v.length );
	return this;
    }

    /**
     * Writes an array of string values to this stream.
     * @param v the array of Strings to be written
     * @return this stream object
     */
    public Ostream put( String[] v )
    {
	out.write_ulong( v.length );
	for ( int i = 0; i < v.length; i++ )
	    out.write_string( v[i] );
	return this;
    }
    
    /**
     * Writes an array of date values to this stream.
     * @param v the array of Dates to be written
     * @return this stream object
     */
    public Ostream put( Date[] v )
    {
	out.write_ulong( v.length );
	for ( int i = 0; i < v.length; i++ )
	    put( v[i] );
	return this;
    }
    
    /**
     * Writes an array of time values to this stream.
     * @param v the array of Times to be written
     * @return this stream object
     */
    public Ostream put( Time[] v )
    {
	out.write_ulong( v.length );
	for ( int i = 0; i < v.length; i++ )
	    put( v[i] );
	return this;
    }
    
    /**
     * Writes an array of Infomation objects to this stream.
     * @param v the array of Infomation objects to be written
     * @param type the IS type of Infomation objects
     * @return this stream object
     */
    public <T extends Info> Ostream put( T[] v, is.Type type )
    {
	out.write_ulong( v.length );
	for ( int i = 0; i < v.length; i++ )
	    put( v[i], type );
	return this;
    }

    /**
     * Writes a vector of objects to this stream.
     * @param vector the vector of objects to be written
     * @param type the IS type of Infomation objects
     * @return this stream object
     */
    public <T extends Info> Ostream put( java.util.Vector<T> vector, is.Type type )
    {
	out.write_ulong( vector.size() );
	for ( int i = 0; i < vector.size(); i++ )
	    put( vector.get(i), type );
	return this;
    }
}
