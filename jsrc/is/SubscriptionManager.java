package is;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import ers.Logger;

class SubscriptionManager
{
    private static void deactivateCallback( is.callback callback )
    {
        try {
            ipc.Core.getRootPOA().deactivate_object( ipc.Core.getRootPOA().reference_to_id( callback ) );
        }
        catch( org.omg.PortableServer.POAPackage.ObjectNotActive ex ) {}
        catch( org.omg.PortableServer.POAPackage.WrongAdapter ex ) {}    
        catch( org.omg.PortableServer.POAPackage.WrongPolicy ex ) {}    
    }

    private static abstract class Subscription
    {
        long time_of_subscription = 0;
        String name;
       	
	abstract void subscribe() throws RepositoryNotFoundException, InvalidCriteriaException;
	
	abstract void unsubscribe() throws SubscriptionNotFoundException, RepositoryNotFoundException;
	
	void resubscribe(long server_startup_time) {
            if (time_of_subscription < server_startup_time) {
                ers.Logger.debug(2, "Restoring the '" + name + "' subscription");
                try {
                    subscribe();
                } 
                catch (Exception ex) {
                    ers.Logger.debug(2, "Got exception trying to resibscribe: " + ex);                       
                }    
                ers.Logger.debug(2, "Subscription '" + name + "' has been restored");
           }
	}
    }

    private static class SimpleSubscription extends Subscription
    {
	private final ipc.Partition partition;
	private final is.callback callback;
	private final int event_mask;

	SimpleSubscription(ipc.Partition p, String n, Callback cb)
		throws RepositoryNotFoundException
	{
	    this.partition = p;
	    this.name = n;
	    this.callback = cb.objectReference();
	    this.event_mask = cb.eventMask();
	    try {
		subscribe();
	    }
	    catch (Exception ex) {
		deactivateCallback(callback);
		throw ex;
	    }
	}

	public void subscribe()
		throws RepositoryNotFoundException
	{
	    is.repository repository = Repository.resolve(partition, name);
	    try {
		repository.add_callback(Repository.getObjectName(name), callback, event_mask);
		time_of_subscription = (new Date()).getTime()/1000;		
	    }
	    catch (org.omg.CORBA.SystemException ex) {
		throw new is.RepositoryNotFoundException(partition, Repository.getServerName(name));
	    }
	    catch (is.InvalidCallback ex) {
		throw new is.RepositoryNotFoundException(
			"IS repository '" + Repository.getServerName(name) + "' from the '" 
		        	+ partition.getName() 
		        	+ "' partition can't communicate with this subscriber, check network configuration");
	    }
	}
	
	public void unsubscribe()
		throws SubscriptionNotFoundException, RepositoryNotFoundException
	{
	    try {
		is.repository repository = Repository.resolve(partition, name);
		repository.remove_callback(Repository.getObjectName(name), callback);
	    }
	    catch ( org.omg.CORBA.SystemException ex ) {
		throw new is.RepositoryNotFoundException(partition, Repository.getServerName(name));
	    }
	    catch ( is.SubscriptionNotFound ex ) {
		throw new is.SubscriptionNotFoundException(name);
	    }
	    finally {
		deactivateCallback(callback);
	    }
	}
    }

    private static class CriteriaSubscription extends Subscription
    {
	private final ipc.Partition partition;
	private final Criteria criteria;
	private final is.callback callback;
	private final int event_mask;

	CriteriaSubscription(ipc.Partition p, String n, Criteria c, Callback cb)
			throws RepositoryNotFoundException, InvalidCriteriaException
	{
	    this.partition = p;
	    this.name = n;
	    this.criteria = c;
	    this.callback = cb.objectReference();
	    this.event_mask = cb.eventMask();
	    try {
		subscribe();
	    }
	    catch (Exception ex) {
		deactivateCallback(callback);
		throw ex;
	    }
	}

	public void subscribe()
		throws RepositoryNotFoundException, InvalidCriteriaException
	{
	    try {
		is.repository repository = Repository.resolve(partition, name);
		repository.subscribe(criteria.criteria, callback, event_mask);
                time_of_subscription = (new Date()).getTime()/1000;               
	    }
	    catch (org.omg.CORBA.SystemException ex) {
		throw new is.RepositoryNotFoundException(partition, name);
	    }
	    catch (is.InvalidCriteria ex) {
		throw new is.InvalidCriteriaException(criteria);
	    }
	    catch (is.InvalidCallback ex) {
		throw new is.RepositoryNotFoundException(
			"IS repository '" + name + "' from the '" 
		        	+ partition.getName() 
		        	+ "' partition can't communicate with this subscriber, check network configuration");
	    }
	}
	
	public void unsubscribe()
		throws SubscriptionNotFoundException, RepositoryNotFoundException
	{
	    try {
		is.repository repository = Repository.resolve(partition, name);
		repository.unsubscribe(criteria.criteria, callback);
	    }
	    catch ( org.omg.CORBA.SystemException ex ) {
		throw new is.RepositoryNotFoundException(partition, name);
	    }
	    catch ( is.SubscriptionNotFound ex ) {
		throw new is.SubscriptionNotFoundException(name, criteria);
	    }
	    finally {
		deactivateCallback(callback);
	    }
	}
    }

    private static class Server
    {
	private final ipc.Partition partition;
	private final String name;
	Map<String, Subscription> subscriptions = new ConcurrentHashMap<String, Subscription>();
	
	Server(ipc.Partition p, String n)
	{
	    partition = p;
	    name = n;
	}
	
	void addSubscription(String n, Callback cb)
		throws RepositoryNotFoundException
	{
	    subscriptions.put(n + "/" + cb.toString(),
			new SimpleSubscription(partition, n, cb));
	}

	void addSubscription(Criteria c, Callback cb)
		throws RepositoryNotFoundException, InvalidCriteriaException
	{
	    subscriptions.put(c.toString() + "/" + cb.toString(),
			new CriteriaSubscription(partition, name, c, cb));
	}
	
	void removeSubscription(String n, Callback cb)
		throws SubscriptionNotFoundException, RepositoryNotFoundException
	{
	    Subscription s = subscriptions.remove(n + "/" + cb.toString());
	    s.unsubscribe();
	}

	void removeSubscription(Criteria c, Callback cb)
		throws SubscriptionNotFoundException, RepositoryNotFoundException
	{
	    Subscription s = subscriptions.remove(c.toString() + "/" + cb.toString());
	    s.unsubscribe();
	}
	
	void checkup(long period)
	{
	    ipc.servantPackage.ApplicationContext acontext;
	    try {
		is.repository repository = Repository.resolve(partition, name);
		acontext = repository.app_context();
		ers.Logger.debug(2, "The startup time for the '" + name + "' server is " + acontext.time);
	    }
	    catch(Exception ex){
		return;
	    }
	    
	    for (Subscription s : subscriptions.values()) {
                s.resubscribe(acontext.time);
	    }
	}
    }
    
    private static Map<String, Server> servers;
    
    static 
    {
	servers = new ConcurrentHashMap<String, Server>();
	ers.Logger.debug(2, "SubscriptionManager is being initialized");
	Thread t = new Thread() {
	    public void run()
	    {
                ers.Logger.debug(2, "Checkup thread started");
		while (true)
		{
		    final long period = 10000L; 
		    
		    try {
			sleep(period);
		    }
		    catch(java.lang.InterruptedException ex) {
			return;
		    }
        		
		    ers.Logger.debug(2, "Checking servers...");
		    for (Server s : servers.values()) 
		    {
			s.checkup(period);
		    }
		}
	    }
	};
	t.setDaemon(true);
	t.start();
    }
    
    static synchronized void addSubscription(ipc.Partition p, String n, Callback cb)
    	throws RepositoryNotFoundException
    {
	String s = Repository.getServerName( n );
	Server server = servers.get(p.getName() + "/" + s);
	if ( server == null )
	{
	    server = new Server(p, s);
	    servers.put(p.getName() + "/" + s, server);
	}
	server.addSubscription(n, cb);
    }

    static synchronized void addSubscription(ipc.Partition p, String s, Criteria c,
	    Callback cb)
    	throws InvalidCriteriaException, RepositoryNotFoundException
    {
	Server server = servers.get(p.getName() + "/" + s);
	if ( server == null )
	{
	    server = new Server(p, s);
	    servers.put(p.getName() + "/" + s, server);
	}
	server.addSubscription(c, cb);
    }

    static synchronized void removeSubscription(ipc.Partition p, String n, Callback cb) 
	    throws SubscriptionNotFoundException, RepositoryNotFoundException
    {
	Server server = servers.get(p.getName() + "/" + Repository.getServerName( n ));
	try {
	    server.removeSubscription(n, cb);
	}
	catch (java.lang.NullPointerException ex) {
	    throw new SubscriptionNotFoundException(n);
	}
    }

    static synchronized void removeSubscription(ipc.Partition p, String s, Criteria c,
	    Callback cb) throws SubscriptionNotFoundException, RepositoryNotFoundException
    {
	Server server = servers.get(p.getName() + "/" + s);
	try {
	    server.removeSubscription(c, cb);
	}
	catch (java.lang.NullPointerException ex) {
	    throw new SubscriptionNotFoundException(s, c);
	}
    }
}
