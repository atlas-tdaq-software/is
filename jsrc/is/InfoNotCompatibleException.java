package is;

/**
 * Thrown to indicate that the two IS information objects
 * have different types
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class InfoNotCompatibleException extends RuntimeException 
{
    /**
     * Constructs an InfoNotCompatibleException with the specified
     * information name.
     *
     * @param name the information name
     */
    public InfoNotCompatibleException( String name ) 
    {
	super( "The type of the '" + name + 
        	"' information in IS repository does not match the given object type." );
    }
}
