package is;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * An abstract class for the IS information type definition.
 * Any user-defined IS information class must inherit either this or the NamedInfo 
 * class and has to implement the publushGuts and refreshGutes methods defined by 
 * the Streamable interface.
 * @author Sergei Kolos
 * @see NamedInfo
 */
public abstract class Info implements Streamable
{
    private Time    time;
    private Type    type;
    private int	    tag;
    private String  name;
    private byte[]  address;
    private Map<String, Annotation> annotations;     
    
    /**
     * Creates an instance of information of a certain type.
     */
    Info( String name, Type type, is.attributes aa )
    {
	this.name = name;
	this.type = type;
	time = new Time( aa.time );
        tag = aa.tag;
	address = aa.address;
	annotations = new TreeMap<String, Annotation>();
	for (int i = 1; i < aa.ann.length; i += 2) {
	    annotations.put(aa.ann[i-1], new Annotation(aa.ann[i]));
	}
    }    

    /**
     * Creates an instance of information of a certain type.
     */
    Info( Type type )
    {
	time = new Time( );
        tag = 0;
	address = InfoProvider.instance().getAddress();
	name = new String();
	this.type = type;
        annotations = new TreeMap<String, Annotation>();
    }    

    /**
     * Creates an instance of information and assigns a <tt>typename</tt> name to this information type.
     * @param typename name of the information type
     */
    public Info( String typename )
    {
	time = new Time( );
        tag = 0;
	address = InfoProvider.instance().getAddress();
	name = new String();
	type = new Type ( typename, this );
        annotations = new TreeMap<String, Annotation>();
    }    
        
    /**
     * Creates an instance of information for which the type name in unknown.
     * @deprecated Exists for compatibility with the previous IS version.
     */
    public Info( )
    {
	this( Type.UnknownName );
    }    
    
    public Annotation getAnnotation(String name) {
        return annotations.get(name);
    }
    
    public <T> void setAnnotation(String name, T value) {
        annotations.put(name, new Annotation(value));
    }
    
    public Map<String, Annotation> getAnnotations() {
        return annotations;
    }
        
    /**
     * Returns the name of that obejct, which had been used for publishing it to the IS repository.
     * If this object has not been published yet, return empty string.
     * @return the object name
     */
    public String getName( )
    {
	return name;
    }    

    /**
     * Returns the latest modification time for the information.
     * @return the modification time
     */
    public Time getTime()
    {
	return time;
    }

    /**
     * Returns type of the information object.
     * @return the information type
     * @see Type
     */
    public Type getType()
    {
	return type;
    }    
	
    /**
     * Returns the tag associated with this information.
     * @return the tag
     */
    public int getTag()
    {
	return tag;
    }

    /**
     * Sends command to the provider of this information.
     * @param cmd command to be sent
     * @exception is.ProviderNotFoundException corresponding information provider is not running
     * @see InfoProvider
     */
    public void sendCommand( String cmd ) throws is.ProviderNotFoundException
    {
	String full_address = InfoProvider.makeConnectString( address );

	try
	{
	    is.provider prov = ipc.TypeHelper.stringToObject( is.provider.class, full_address );
	    if ( prov == null )
	    {
		throw new is.ProviderNotFoundException(name, full_address);
	    }
	    prov.command( name, cmd );
	}
	catch ( Exception ex )
	{
	    throw new is.ProviderNotFoundException(name, full_address);
	}
    }    
	
    /**
     * Checks existence of the provider for this information.
     * @return true if provider exist, false otherwise
     * @see InfoProvider
     */
    public boolean providerExist( )
    {
	String full_address = InfoProvider.makeConnectString( address );
	
	try
	{
	    is.provider prov = ipc.TypeHelper.stringToObject( is.provider.class, full_address );
	    if ( prov == null || prov._non_existent() )
		return false;
	}
	catch( Exception ex )
	{
	    return false;
	}
        
	return true;
    }    
	
    /**
     * @see Streamable
     */
    public void publishGuts( is.Ostream out )
    {
	time.setTime( java.lang.System.currentTimeMillis() );
	address = InfoProvider.instance().getAddress();
    }
    
    /**
     * @see Streamable
     */
    public void refreshGuts( is.Istream in )
    {
    }
    
    void update(String name, is.info info) throws InfoNotCompatibleException {
        Type type = new Type( info.type );
        if (    !( this instanceof AnyInfo )
             && !type.subTypeOf( this.getType() ) )
        {
            throw new is.InfoNotCompatibleException(name);
        }
        setType(type);    
        
        update(name, info.value);
    }

    void update(String name, is.value value) throws InfoNotCompatibleException {        
        this.refreshGuts( new is.InfoInStream( value.data ) );
        this.address = value.attr.address;
        this.name = name;
        this.time = new Time(value.attr.time);
        this.tag = value.attr.tag;
        this.annotations.clear();
        for (int i = 1; i < value.attr.ann.length; i += 2) {
            this.annotations.put(value.attr.ann[i-1], new Annotation(value.attr.ann[i]));
        }
    }

    String[] flatAnnotations() {
        return annotations.entrySet().stream()
            .map(e -> Arrays.asList(new String[] {e.getKey(), e.getValue().data()}))
            .flatMap(x -> x.stream())
            .toArray(String[]::new);
    }

    byte[] getAddress( )
    {
	return address;
    }
        
    void setName( String name )
    {
	this.name = name;
    }    
    
    void setTag( int tag )
    {
	this.tag = tag;
    }    
	
    void setType( Type type )
    {
	this.type = type;
    }
}
