#include <is/infoany.h>
#include <is/infodocument.h>
#include <Xm/Matrix.h>
#include <string>
#include <sstream>
#include <iomanip>

#include <unistd.h>

#undef Success

namespace ismon
{
    extern Pixel ErrorColor;
    extern Pixel TextBgColor;
}

namespace
{
    void UpdateCell( String ** table, int row, int col, const char * value )
    {
	XtFree( table[row][col] );
	table[row][col] = strdup( value );
    }
}

void print_value ( Widget w, IPCPartition & p, ISType & last_type, ISInfoAny& isa )
{  
    ISType type = isa.type();
    size_t attributes_count = type.entries();
    const ISInfo::Annotations & annotations = isa.annotations();
    int extraRows = !annotations.empty();

    XtAppLock( XtWidgetToApplicationContext( w ) );
    
    if ( type != last_type || last_type == ISType::Null )
    {
    	XbaeMatrixDeleteRows( w, 0, XbaeMatrixNumRows( w ) );
    	XbaeMatrixAddRows( w, 0, 0, 0, 0, attributes_count +  extraRows);
	// Set dummy value to force table allocation
        XbaeMatrixSetCell( w, 0, 0, (char*)" " );
    }
    
    String ** table;
    XtVaGetValues( w, XmNcells, &table, NULL );
        
    ISInfoDocument * isd = 0;
    
    try {
    	isd = new ISInfoDocument( p, isa );
    }
    catch( daq::is::Exception & ) {
    }
    
    if (!annotations.empty()) {
        typedef std::pair<std::string, ISAnnotation> P;
        std::ostringstream out;
        std::for_each(annotations.begin(), annotations.end(), [&out](const P & p) {
            out << "[" << p.first << "=" << p.second <<"]";});
        std::string ann = out.str();
        UpdateCell( table, 0, 0, ann.c_str() );
        UpdateCell( table, 0, 1, "Map<string,any>" );
        UpdateCell( table, 0, 2, "annotations" );
        UpdateCell( table, 0, 3, "This is a service attribute that is not defined by the object type" );

        for (int i = 0; i < 4; ++i)
        {
            XbaeMatrixSetCellBackground(w, 0, i, ismon::TextBgColor);
        }
    }

    for ( size_t i = 0; i < attributes_count; i++ )
    {
	std::ostringstream out;
        int row = i  + extraRows;
        
        ISType::Basic btype = type.entryType(i);
        // Setup more appropriate floating point representation
	out.unsetf( std::ios::showpoint );
        out << std::setprecision( btype == ISType::Float ? 7 : btype == ISType::Double ? 14 : 6 );

        // Can distinguish between dec and hex representations
        out << std::showbase;

	const ISInfoDocument::Attribute * attr = 0;
        if ( isd && isd -> attributeCount() > i )
        {
	    attr = &(isd -> attribute( i ));
        }

	size_t size = is::print_attribute_value_ext( out, isa, type, i, -1, isd );
	std::string value = out.str();
	UpdateCell( table, row, 0, (char*)value.c_str() );
	out << std::dec;
	
	if ( type.entryArray( i ) || type != last_type )
        {
            out.str( "" );
      	    is::print_attribute_type( out, type, i, size );
	    std::string type_name = out.str();

	    UpdateCell( table, row, 1, (char*)type_name.c_str() );
	}
        
        if ( attr && type != last_type )
	{
	    if ( type.entryType( i ) != attr->typeCode() )
	    {
		XbaeMatrixSetCellColor( w, row, 2, ismon::ErrorColor );
		XbaeMatrixSetCellColor( w, row, 3, ismon::ErrorColor );
		std::string descr;
		if ( !attr )
		{
		    descr = "Object has no more attributes defined";
		}
		else
		{
		    descr = "Actual type is not the same as the declared one : ";
		    descr += attr->typeName() + ( attr->isArray() ? "[]" : "" );
		}
		UpdateCell( table, row, 3, (char*)descr.c_str() );
	    }
	    else
	    {
		out.str( "" );
		out << attr->description();
		std::string description = out.str();
		UpdateCell( table, row, 3, (char*)description.c_str() );
	    }

	    out.str( "" );
	    out << attr->name();
	    std::string name = out.str();
	    UpdateCell( table, row, 2, (char*)name.c_str() );
	}	
    }
    // This call is a workaround for the issue of the first cell appearing in edit mode after the update
    XbaeMatrixCancelEdit( w, true );

    XbaeMatrixRefresh( w );
    XtAppUnlock( XtWidgetToApplicationContext( w ) );

    last_type = isa.type();
    
    delete isd;
}
