#ifndef IS_INFO_DIALOG_H
#define IS_INFO_DIALOG_H

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>

#include <mutex>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>

#include <updatequeue.h>
#include <Xm/Xm.h>

using namespace boost::multi_index;

typedef char** Row;

struct RowHasher
{
   std::size_t operator()(char** const & x) const
   {  
      std::size_t seed = 0;
      char *s = x[0];
      for(; *s; ++s)   boost::hash_combine(seed, *s);
      return seed;
   }
   
   std::size_t operator()(const char** const & x) const
   {  
      std::size_t seed = 0;
      const char *s = x[0];
      for(; *s; ++s)   boost::hash_combine(seed, *s);
      return seed;
   }
};

struct RowEqual
{
   bool operator()(const char** const & x, char** & y) const
   {  
      return !std::strcmp(x[0],y[0]);
   }
   
   bool operator()(char** const & x, char** & y) const
   {  
      return !std::strcmp(x[0],y[0]);
   }
};

typedef boost::multi_index_container<
    char**,
    indexed_by<
	hashed_unique<  identity<char**>,
			RowHasher,
			RowEqual
    	>
    >
> TableMap;

class InfoDialog : public boost::noncopyable
{
  public:
    InfoDialog( const IPCPartition & pp, const char * server );
    ~InfoDialog();
    
    void AddInfo( const std::string & name, const OWLTime & time, const ISType & type );
    void ApplyFilters( int row );
    void AskConfirmation( );
    
    void DisplayAttributeValue( int row, int column );
    
    Widget GetLogWidget() { return log1; }
    
    void RemoveInfo( const std::string & name );
    void RemoveSelectedInfo( );
    void SetLogText( int number, int val1, int val2 = 0 );
    void SendCommand( );
    void SelectInfo( int row );
    void SetFilter( int column, const char * filter );
    void UpdateInfo( const std::string & name, const OWLTime & time );
    
  private:
    void fillInfoList( );
    void callback( ISCallbackEvent * isi );
    int  readInfo( );

  private:
    Widget shell;
    Widget table;
    Widget value_table;
    Widget value;
    Widget log1, log2, log3, log4;
    Widget exit_button, delete_button, send_button, command_field;
    
    int rows_total;
    int rows_selected;
    int selected_row;
    
    IPCPartition partition;
    ISInfoDictionary dictionary;
    ISInfoReceiver receiver;
    std::string server_name;
    
    ISType last_type;
    std::string filters[4];
    UpdateQueue * uqueue;
    TableMap map;
    std::mutex m_mutex;
};

#endif
