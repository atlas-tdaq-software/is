#include <stdlib.h>

#include <Xm/Xm.h>
#include <Xm/Matrix.h>
#include <Xm/PanedW.h>
#include <Xm/TextF.h>
#include <Xm/Form.h>
#include <Xm/Label.h>

#include <ismonitor.h>

#include <iostream>

namespace ismon
{
    extern Pixel 	LineColor;
    extern Pixel 	OddBgColor;
    extern Pixel 	EvenBgColor;
    extern Pixel 	BgColor;
    extern Pixel 	TextBgColor;
    extern Pixel 	TextColor;
    extern XmFontList	TableCaptionFont;
}

extern Cursor ResizeCursor;

extern void selectServerListCallback(Widget w, XtPointer client, XtPointer call);
extern void selectInfoListCallback(Widget w, XtPointer client, XtPointer call);	
extern void buttonShowPressed(Widget w, XtPointer client, XtPointer call);	
extern void selectValueCellCallback(Widget w, XtPointer user, XtPointer call);
extern void valueChangedCallback( Widget w, XtPointer user, XtPointer cb );
extern void sortRowsCallback( Widget w, XtPointer, XtPointer cb );

void enterCellCallback( Widget , XtPointer, XtPointer cb )
{
    XbaeMatrixEnterCellCallbackStruct *cbs = (XbaeMatrixEnterCellCallbackStruct *)cb;
    cbs->doit = False;
    cbs->map = False;
}

inline void swap_cells( Widget w, String ** cells, int num1, int num2 )
{
    String * ptr = cells[num1];
    cells[num1] = cells[num2];
    cells[num2] = ptr;

    int height = XbaeMatrixGetRowHeight( w, num1 );
    XbaeMatrixSetRowHeightEx( w, num1, XbaeMatrixGetRowHeight( w, num2 ) );
    XbaeMatrixSetRowHeightEx( w, num2, height );

    String tmp = cells[num1][4];
    cells[num1][4] = cells[num2][4];
    cells[num2][4] = tmp;
}

void quick_sort( Widget w, String ** cells, const int column, int start, int end, bool ascending )
{
    while ( end > start + 1 )
    {
	int piv = start + ( end - start )/ 2;
	swap_cells( w, cells, piv, start );
        
        char * pivot = cells[start][column];
	bool dupHandler = false;
        int index = start;
        for ( int i = start + 1; i < end; ++i )
	{
	    int cv = strcmp( cells[i][column], pivot );
            if ( !ascending )
            	cv = 0 - cv;
                
            if ( cv < 0 || ( !cv && (dupHandler=!dupHandler) ) )
		swap_cells( w, cells, ++index, i );
	}
	swap_cells( w, cells, index, start );
	if( index - start < end - index - 1 ) {
            quick_sort( w, cells, column, start, index, ascending );
            start = index + 1;
        }
        else {
	    quick_sort( w, cells, column, index + 1, end, ascending );
            end = index;
        }
    }
}

void sortRowsCallback( Widget w, XtPointer, XtPointer cb )
{
    XbaeMatrixLabelActivateCallbackStruct *cbs = (XbaeMatrixLabelActivateCallbackStruct *)cb;
    
    String ** cells;
    
    XtVaGetValues( w, XmNcells, &cells, NULL );
    int num = XbaeMatrixNumRows( w );
    
    bool ascending = true;
    String * labels;
    XtVaGetValues( w, XmNcolumnLabels, &labels, NULL );
    for ( int k = 0; k < XbaeMatrixNumColumns( w ); k++ )
    {
	char * sorting_order = labels[k] + strlen(labels[k]) - 6;
	if ( k == cbs->column ) {
            if ( !strcmp( sorting_order, "(A->z)" ) ) {
            	ascending = false;
                strcpy( sorting_order, "(Z->a)" );
            }
            else {
                strcpy( sorting_order, "(A->z)" );
            }
        }
	else {
	    strcpy( sorting_order, "      " );
        }
    }
    
    quick_sort( w, cells, cbs->column, 1, num, ascending );
    
    XtVaSetValues( w, XmNcolumnLabels, labels, NULL );
    
    XbaeMatrixSetRowHeight( w, 0, -1 );
    XbaeMatrixRefresh( w );
}

void MouseMotionOverTable( Widget w, XEvent * event, String * , Cardinal * )
{
    static bool resize_cursor_set;
    int x, y, mx, mmx;
    int row, column;
    int fuzzy = 4;
    int columns;
    
    XtVaGetValues( w, XmNcolumns, &columns, NULL );
    
    if (!XbaeMatrixEventToXY(w, event, &x, &y))
	return;
        
    if (!XbaeMatrixGetEventRowColumn( w, event, &row, &column ))
	return;
        
    if ( row < 0 || column < 0 )
    	return;
        
    if (!XbaeMatrixRowColToXY( w, row, column, &mx, &y ))
	return;
    
    mmx = mx;
    if ( column < columns-1 && !XbaeMatrixRowColToXY( w, row, column + 1, &mmx, &y ) )
	return;
                
    if ( ::abs( x - mx ) < fuzzy || ::abs( x - mmx ) < fuzzy )
    {
	if ( !resize_cursor_set )
	{
	    resize_cursor_set = true;
	    XDefineCursor( XtDisplay(w), XtWindow(w), ResizeCursor );
	    XmUpdateDisplay(w);
	}
    }
    else if ( resize_cursor_set )
    {
	resize_cursor_set = false;
	XUndefineCursor( XtDisplay(w), XtWindow(w) );
	XmUpdateDisplay(w);
    }
}

void setupTable( Widget w, bool value_table = false )
{
    Arg al[64];
    Widget scrollbar;
    
    int ac = 0;
    XtSetArg(al[ac], XmNverticalScrollBar, &scrollbar); ac++;
    XtGetValues(w, al, ac );
    ac = 0;
    XtSetArg(al[ac], XmNwidth, 12); ac++;
    XtSetArg(al[ac], XmNshadowThickness, 1); ac++;
    XtSetArg(al[ac], XmNforeground, ismon::OddBgColor); ac++;
    XtSetValues ( scrollbar, al, ac );
    
    if ( value_table )
	XtVaSetValues( w, XmNoddRowBackground, ismon::OddBgColor, 
			XmNevenRowBackground, ismon::OddBgColor,
			XmNbackground, ismon::BgColor,
			NULL );
    else
	XtVaSetValues( w, XmNoddRowBackground, ismon::OddBgColor, 
			XmNevenRowBackground, ismon::EvenBgColor,
			XmNbackground, ismon::BgColor,
			XmNbuttonLabelBackground, ismon::BgColor, 
			NULL );
}

Widget createSearchPanel( Widget parent, XtPointer info, int column )
{
    int height = XbaeMatrixGetRowHeight( parent, 0 );
    Arg al[20];
    int ac = 0;
    XtSetArg(al[ac], XmNshadowThickness, 0); ac++;
    XtSetArg(al[ac], XmNheight, height); ac++;
    XtSetArg(al[ac], XmNwidth, 3000); ac++;
    XtSetArg(al[ac], XmNtopAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNtopOffset, 0); ac++;
    XtSetArg(al[ac], XmNbottomAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNbottomOffset, 0); ac++;
    XtSetArg(al[ac], XmNleftAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNleftOffset, 0); ac++;
    XtSetArg(al[ac], XmNrightAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNrightOffset, 0); ac++;
    Widget form = XmCreateForm ( parent, (char *) "form", al, ac );
    XtManageChild( form );
    
    ac = 0;
    XtSetArg(al[ac], XmNhighlightThickness, 0); ac++;
    XtSetArg(al[ac], XmNshadowThickness, 0); ac++;
    XtSetArg(al[ac], XmNlabelType, XmPIXMAP); ac++;    
    XtSetArg(al[ac], XmNlabelPixmap, pixmap_resources.SearchIcon ); ac++;
    XtSetArg(al[ac], XmNtopAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNtopOffset, 1); ac++;
    XtSetArg(al[ac], XmNbottomAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNbottomOffset, 1); ac++;
    XtSetArg(al[ac], XmNleftAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNleftOffset, 1); ac++;
    XtSetArg(al[ac], XmNrightAttachment, XmATTACH_NONE); ac++;
    XtSetArg(al[ac], XmNbackground, ismon::TextBgColor); ac++;
    Widget icon = XmCreateLabel ( form, (char *) "icon", al, ac );
    XtManageChild( icon );
   
    ac = 0;
    XtSetArg(al[ac], XmNhighlightThickness, 0); ac++;
    XtSetArg(al[ac], XmNshadowThickness, 1); ac++;
    XtSetArg(al[ac], XmNmarginHeight, 4); ac++;
    XtSetArg(al[ac], XmNmarginWidth, 27); ac++;
    XtSetArg(al[ac], XmNresizeWidth, FALSE); ac++;
    XtSetArg(al[ac], XmNtopAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNtopOffset, 0); ac++;
    XtSetArg(al[ac], XmNbottomAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNbottomOffset, 0); ac++;
    XtSetArg(al[ac], XmNleftAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNleftOffset, 0); ac++;
    XtSetArg(al[ac], XmNrightAttachment, XmATTACH_FORM); ac++;
    XtSetArg(al[ac], XmNrightOffset, 0); ac++;
    XtSetArg(al[ac], XmNbackground, ismon::TextBgColor); ac++;
    XtSetArg(al[ac], XmNforeground, ismon::TextColor); ac++;
    XtSetArg(al[ac], XmNuserData, column); ac++;
    Widget search = XmCreateTextField ( form, (char *) "search", al, ac );
    XtManageChild( search );

    XtAddCallback( search, XmNvalueChangedCallback, valueChangedCallback, info );
    
    return form;
}

Widget CreateInfoTable( Widget parent, XtPointer info )
{
    static char * labels[] = {	(char *)"Name       ", 		
    				(char *)"Type       ", 
    				(char *)"Modified       ",	
                                (char *)"Description       ", 
                                (char *)"Row       " };
    static short  widths[] = { 90, 20, 25, 700, 0 };
    
    static XtTranslations    translations = XtParseTranslationTable( "#override\n"
		"<Key>osfPageDown:	SelectCell(PageDown)\n"
		"<Key>osfPageUp:	SelectCell(PageUp)\n"
		"<Key>osfDown:		SelectCell(Down)\n"
		"<Key>osfUp:		SelectCell(Up)\n"
		"<Btn1Down>:		SelectCell() ResizeColumns()\n"
		"<Motion>:		MouseMotionOverTable()" );

    Widget w = XtVaCreateManagedWidget(
	"mw", xbaeMatrixWidgetClass, parent,
	XmNrows, 1,
	XmNvisibleRows, 8,
	XmNcolumns, 5,
        XmNfixedRows, 1,
        XmNfill, TRUE,
        XmNtraverseFixedCells, TRUE,
	XmNvisibleColumns, 4,
	XmNallowColumnResize, TRUE,
	XmNallowRowResize, FALSE,
	XmNcellHighlightThickness, 0,
	XmNcellShadowThickness, 1,
	XmNshadowThickness, 2,
	XmNborderWidth, 0,
        XmNtraversalOn, FALSE,
	XmNboldLabels, FALSE,
	XmNbuttonLabels, TRUE,
        XmNlabelFont, ismon::TableCaptionFont,
	XmNgridType, XmGRID_COLUMN_SHADOW,
	XmNgridLineColor, ismon::LineColor,
	XmNcolumnLabels, labels,
	XmNcolumnWidths, widths,
	XmNtranslations, translations,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNtopAttachment, XmATTACH_FORM,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNleftOffset, 0,
	XmNrightOffset, 0,
        XmNselectScrollVisible, FALSE,
	XmNhorizontalScrollBarDisplayPolicy, XmDISPLAY_NONE,
	XmNuserData, info,
	NULL);
        
    XtAddCallback( w, XmNenterCellCallback, enterCellCallback, (XtPointer)0 );
    XtAddCallback( w, XmNselectCellCallback, selectInfoListCallback, (XtPointer)info );
    XtAddCallback( w, XmNlabelActivateCallback, sortRowsCallback, (XtPointer) 0 );
    
    XbaeMatrixSetCellWidget( w, 0, 0, createSearchPanel( w, info, 0 ) );
    XbaeMatrixSetCellWidget( w, 0, 1, createSearchPanel( w, info, 1 ) );
    XbaeMatrixSetCellWidget( w, 0, 2, createSearchPanel( w, info, 2 ) );
    XbaeMatrixSetCellWidget( w, 0, 3, createSearchPanel( w, info, 3 ) );
        
    setupTable( w );	
    
    return w;
}

Widget CreateInfoValueTable( Widget parent, XtPointer info )
{
    static char * labels[] = { (char *)"Value", (char *)"Type", (char *)"Name", (char *)"Description" };
    static short  widths[] = { 80, 10, 20, 500 };
    static XtTranslations    translations = XtParseTranslationTable( "#override\n"
			"<Btn1Down> : SelectCell(Select) ResizeColumns()\n"
			"<Motion>   : MouseMotionOverTable()" );

    Widget table = XtVaCreateManagedWidget(
	"mw", xbaeMatrixWidgetClass, parent,
	XmNrows, 0,
	XmNcolumns, 4,
	XmNallowColumnResize, TRUE,
        XmNfill, TRUE,
	XmNallowRowResize, FALSE,
	XmNcellHighlightThickness, 1,
	XmNcellShadowThickness, 1,
	XmNshadowThickness, 1,
	XmNborderWidth, 0,
        XmNtraversalOn, FALSE,
	XmNboldLabels, FALSE,
	XmNbuttonLabels, FALSE,
        XmNlabelFont, ismon::TableCaptionFont,
	XmNgridType, XmGRID_CELL_LINE,
	XmNgridLineColor, ismon::LineColor,
	XmNcolumnLabels, labels,
	XmNcolumnWidths, widths,
	XmNtranslations, translations,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNtopAttachment, XmATTACH_FORM,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNleftOffset, 0,
	XmNrightOffset, 0,
	XmNheight, 250,
	XmNhorizontalScrollBarDisplayPolicy, XmDISPLAY_NONE,
	NULL);

    setupTable( table, true );
    XtAddCallback( table, XmNselectCellCallback, selectValueCellCallback, (XtPointer)info );
    
    return table;
}

Widget CreateServerTable( Widget parent )
{
    static char * labels[] = {	(char *)"Name       ", 
    				(char *)"Started       ",
                                (char *)"Host       ",
                                (char *)"Owner       ",
                                (char *)"Pid       " };
    static short  widths[] = { 60, 16, 15, 15, 500 };
    static XtTranslations    translations = XtParseTranslationTable( "#override\n"
		"<Btn1Down> : DefaultAction() SelectCell(Select) ResizeColumns()\n"
		"<Motion>   : MouseMotionOverTable()" );

    Widget w = XtVaCreateManagedWidget(
	"mw", xbaeMatrixWidgetClass, parent,
	XmNrows, 0,
	XmNcolumns, 5,
	XmNallowColumnResize, TRUE,
	XmNallowRowResize, FALSE,
	XmNcellHighlightThickness, 0,
	XmNcellShadowThickness, 1,
	XmNshadowThickness, 1,
	XmNborderWidth, 0,
        XmNtraversalOn, FALSE,
	XmNboldLabels, FALSE,
	XmNbuttonLabels, TRUE,
        XmNlabelFont, ismon::TableCaptionFont,
	XmNgridType, XmGRID_COLUMN_LINE,
	XmNgridLineColor, ismon::LineColor,
	XmNcolumnLabels, labels,
	XmNcolumnWidths, widths,
	XmNtranslations, translations,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNtopAttachment, XmATTACH_FORM,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNleftOffset, 0,
	XmNrightOffset, 0,
	XmNhorizontalScrollBarDisplayPolicy, XmDISPLAY_NONE,
	NULL);    
    setupTable( w );

    XtAddCallback( w, XmNselectCellCallback, selectServerListCallback, (XtPointer) 0 );
    XtAddCallback( w, XmNlabelActivateCallback, sortRowsCallback, (XtPointer) 0 );
    XtAddCallback( w, XmNdefaultActionCallback, buttonShowPressed, (XtPointer) 0 );
	
    return w;
}
