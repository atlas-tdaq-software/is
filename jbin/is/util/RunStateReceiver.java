package is.util;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.HashMap;

class RunStateReceiver extends InfoReceiver
{
     private static HashMap<String,Color> state_colors;
     private Color bg_color;
    
     static
     {
	state_colors = new HashMap<String,Color>();
        state_colors.put( "ABSENT", Color.gray );
	state_colors.put( "UNKNOWN", Color.gray );
        state_colors.put( "NONE", Color.lightGray );
        state_colors.put( "BOOTED", Color.lightGray );
	state_colors.put( "INITIAL", Color.cyan );
	state_colors.put( "CONFIGURED", Color.yellow );
	state_colors.put( "CONNECTED", Color.orange );
	state_colors.put( "DCSTOPPED", Color.green );
	state_colors.put( "HLTSTOPPED", Color.green );
	state_colors.put( "ROIBSTOPPED", Color.green );
	state_colors.put( "SFOSTOPPED", Color.green );
        state_colors.put( "GTHSTOPPED", Color.green );
	state_colors.put( "READY", Color.green );
	state_colors.put( "PAUSED", Color.green );
	state_colors.put( "RUNNING", Color.green );
	state_colors.put( "UP", Color.green );
        state_colors.put( "*", Color.white );
    };
   
    public RunStateReceiver( ipc.Partition partition, Color bg_color )
    {
	super( partition, "RunCtrl.RootController", 
        		new int[] { 0, 3 }, new String[] { "Run State", "Error" }, bg_color );
	
	this.bg_color = bg_color;
	state_colors.put( UNDEFINED, bg_color );
    }

    protected void update( int pos, Object v )
    {
	if (pos == 0)
	{
	    try {
		Color c = state_colors.get( (String)v );
		values[pos].setBackground( c == null ? state_colors.get( "*" ) : c );
	    }
	    catch( Exception ex ) {
	    }
	    
	    super.update( pos, v );
	}
	else
	{
	    String err = "";
	    try {
		String errors[] = (String[])v;
		for (int i = 0; i < errors.length; ++i)
		    err += errors[0] + "; ";
		values[pos].setBackground( err.length() == 0 ? bg_color : Color.red );
	    }
	    catch (ClassCastException ex) {
		// do nothing;
	    }
	    super.update( pos, err );
	}
    }
}

