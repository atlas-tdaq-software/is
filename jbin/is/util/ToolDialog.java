package is.util;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;

public class ToolDialog extends JPanel
{
    private class TwoStateButton extends JButton
    {
   	ImageIcon selected_icon;
   	ImageIcon unselected_icon;
        boolean selected;
        
        TwoStateButton( ImageIcon sicon, ImageIcon uicon )
        {
            super( uicon );
            selected_icon = sicon;
            unselected_icon = uicon;
            selected = false;
	    addMouseListener(new MouseAdapter()  {
	    public void mousePressed(MouseEvent e) {
		if( e.getClickCount() == 1 )
		    stateChanged();
	     }
	    });
        }
        
        public boolean isSelected()
        {
            return selected;
        }
        
        public void setSelected( boolean state )
        {
            if ( selected != state )
            	stateChanged();
        }
        
        void stateChanged()
        {
	    selected = !selected;
            if ( selected )
	    {
                setIcon( selected_icon );
	    }
	    else
	    {
                setIcon( unselected_icon );
	    }
        }
    }
   
    private class Title extends JButton implements MouseListener, MouseMotionListener
    {
        private Point origin;
        
        Title( String text )
        {
            super( text );
	    addMouseListener( this );
	    addMouseMotionListener( this );
        }

	public void mouseClicked(MouseEvent e) { }
	public void mouseEntered(MouseEvent e) { }
	public void mouseExited(MouseEvent e) { }
	public void mouseReleased(MouseEvent e) { }
        
	public void mousePressed(MouseEvent e)
        {
            origin = e.getPoint();
	}
        
	public void mouseDragged(MouseEvent e)
        {
            Point event = e.getPoint();
            int shiftx = ( event.x - origin.x );
            int shifty = ( event.y - origin.y );
            
            Point location = window.getLocation();
            window.setLocation( location.x + shiftx, location.y + shifty );
	}
        
	public void mouseMoved(MouseEvent e)
        {
	}
    }
   
    class ToolBar extends JToolBar
    {                
        ToolBar( )
        {
            setBorderPainted( false );
	    setFloatable( false );
	    setLayout( new GridBagLayout( ) );
            
            java.awt.GridBagConstraints constraints = new java.awt.GridBagConstraints();
	    constraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
	    constraints.weightx = 100.0;
            setup( title );
            add ( title, constraints );
            addSystemTools();
        }
        
    	private void setup( JButton button )
        {
	    button.setAlignmentY( Component.TOP_ALIGNMENT );
	    button.setMargin( new java.awt.Insets( 0, 0, 0, 0 ) );
	    button.setBorderPainted( false );
	    button.setFocusPainted( false );
	    button.setOpaque( false );
        }
        
        private void addSystemTools()
        {
            setup( alwaysOnTopButton );
            setup( closeDialogButton );
            java.awt.GridBagConstraints constraints = new java.awt.GridBagConstraints();
	    constraints.weightx = 1.0;
	    add( alwaysOnTopButton, constraints );
            add( closeDialogButton, constraints );
        }
    }
    
    private JButton closeDialogButton = new JButton( new ImageIcon(getClass().getResource("/images/close.gif") ) );
    private TwoStateButton alwaysOnTopButton = new TwoStateButton( 
    				new ImageIcon(getClass().getResource("/images/ontop.gif") ),
                                new ImageIcon(getClass().getResource("/images/offtop.gif") ) );
    private Window window;
    private Dimension dimension;
    private Title title;
    
    protected ToolBar toolbar;
    
    /**Construct the frame*/
    public ToolDialog( Window window, String title )
    {
        super( new BorderLayout( ) );
        this.window = window;
        this.title = new Title( title );
        toolbar = new ToolBar( );
	
	try
        {
	    jbInit( );
	}
	catch(Exception e) {
	    e.printStackTrace();
	}
    }
    
    /**Component initialization*/
    private void jbInit( ) throws Exception
    {
	setBorder( BorderFactory.createEtchedBorder(EtchedBorder.LOWERED) );
        
        closeDialogButton.addActionListener(new ActionListener()  {
         public void actionPerformed(ActionEvent e) {
           closeDialogActionPerformed(e);
         }
        });
        closeDialogButton.setToolTipText( "Close application" );
        
        alwaysOnTopButton.addActionListener(new ActionListener()  {
	 public void actionPerformed(ActionEvent e) {
	   alwaysOnTopActionPerformed();
	 }
	});
	alwaysOnTopButton.setToolTipText("Can be overlapped by other windows");
	       
	add(toolbar, BorderLayout.NORTH);
    }
    
    private void closeDialogActionPerformed(ActionEvent e)
    {
        System.exit( 0 );
    }
    
    private void alwaysOnTopActionPerformed()
    {
    	if ( alwaysOnTopButton.isSelected() )
        {
	    alwaysOnTopButton.setToolTipText("Stay above all other windows");
            window.setAlwaysOnTop( true );
        }
        else
	{
	    alwaysOnTopButton.setToolTipText("Can be overlapped by other windows");
            window.setAlwaysOnTop( false );
        }
    }
}

