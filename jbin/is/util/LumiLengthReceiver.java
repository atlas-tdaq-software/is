package is.util;

import java.awt.*;
import java.util.*;
import javax.swing.JLabel;

class LumiLengthReceiver extends InfoReceiver
{
    private static final java.text.SimpleDateFormat time_formatter
		 = new java.text.SimpleDateFormat( "HH:mm:ss", java.util.Locale.US );
                 
    LumiLengthReceiver( ipc.Partition partition, Color bg_color )
    {
	super( partition, "RunParams.LBSettings", 
        	new int[] { 1 }, new String[] { "Lumi Block Length" }, bg_color );
    }

    protected void updateAll( is.AnyInfo info )
    {                
	try {
	    String discriminator = (String)info.getAttribute( 0 );
	    String length = info.getAttribute( 1 ).toString();
	    String v = length + ( discriminator.equals( "TIME" ) ? " seconds" : " events" );
            update( 0, v );
	}
	catch( Exception ex ) {
	    super.updateAll( info );
	}        	
    }
}

