package is.util;

public interface PartitionStateListener
{
    void stopped();
    
    void started();
}
