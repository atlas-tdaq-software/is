package is.tool;


abstract class BatchProcessor
{
    public static class InvalidOptions extends Exception
    {
        private static final String hint = "HINT: type 'is_generator.sh -h' for list of options";
    	private String reason;
        
        InvalidOptions( String reason )
        {
            this.reason = reason;
        }
        
        void print()
        {
            System.err.println( reason );
            System.err.println( hint );
        }
    };
    
    static boolean generate_java, generate_cpp, use_simple_info, use_vector, no_namespace;
    static String  directory;
    static java.util.Vector<String>  class_names;
    static boolean batch_job_found;
    static boolean generate_simple, generate_named;
    
    static
    {
	generate_java = generate_cpp = false;
	directory = (new java.io.File( "" )).getAbsolutePath();
        class_names = new java.util.Vector<String>();
	batch_job_found = false;
        generate_simple = generate_named = false;
    }
    
    static public void process( String[] args ) throws InvalidOptions
    {
	int i;
	CodeProducer.Options options = new CodeProducer.Options();
	for ( i = 0; i < args.length; i++ )
	{
	    if ( args[i].equals( "-h" ) || args[i].equals( "--help" ) )
	    {
		printUsage();
                return;
	    }
	    if( args[i].equals( "-np" ) || args[i].equals( "--no-print-function" ) )
	    {
		options.with_print_function = false;
		continue;
	    }
	    if( args[i].equals( "-d" ) || args[i].equals( "--outdir" ) )
	    {
		i++;
		if ( i >= args.length )
		{
		    throw new InvalidOptions( "ERROR: no value specified for -d parameter" );
		}
		directory = args[i];
		continue;
	    }
	    if( args[i].equals( "-p" ) || args[i].equals( "--package" ) )
	    {
		i++;
		if ( i >= args.length )
		{
		    throw new InvalidOptions( "ERROR: no value specified for -p parameter" );
		}
		options.namespaces.add( args[i] );
		continue;
	    }
	    if( args[i].equals( "-pd" ) || args[i].equals( "--prefix" ) )
	    {
		i++;
		if ( i >= args.length )
		{
		    throw new InvalidOptions( "ERROR: no value specified for -p parameter" );
		}
		options.directories.add( args[i] );
		continue;
	    }
	    if( args[i].equals( "-cn" ) || args[i].equals( "--class-name" ) )
	    {
		i++;
		if ( i >= args.length )
		{
		    throw new InvalidOptions( "ERROR: no value specified for " + args[i-1] + "  parameter" );
		}
		class_names.add( args[i] );
		continue;
	    }
	    if( args[i].equals( "-java" ) || args[i].equals( "--java" ) )
	    {
		generate_java = true;
		continue;
	    }
	    if( args[i].equals( "-cpp" ) || args[i].equals( "--cpp" ) )
	    {
		generate_cpp = true;
		continue;
	    }
	    if( args[i].equals( "-n" ) || args[i].equals( "--named" ) )
	    {
		generate_named = true;
		continue;
	    }
	    if( args[i].equals( "-s" ) || args[i].equals( "--simple" ) )
	    {
		generate_simple = true;
		continue;
	    }
	    if ( args[i].equals( "-a" ) || args[i].equals( "--array" ) )
	    {
		options.with_vector = false;
		continue;
	    }
	    if( args[i].equals( "-nn" ) || args[i].equals( "--no-namespace" ) )
	    {
		options.with_namespace = false;
		continue;
	    }
	    if( args[i].startsWith( "-" ) || args[i].startsWith( "--" ) )
	    {
		throw new InvalidOptions( "ERROR: Invalid option \"" + args[i] + "\"" );
            }
	    break;
	}
	
	if ( i >= args.length )
	{
	    throw new InvalidOptions( "ERROR: No input XML files have been defined" );
	}
	
        Repository repository = new Repository( );
	for ( ; i < args.length; i++ )
	{
	    repository.load( args[i] );
	}
	
	if ( !generate_cpp && !generate_java )
	{
	    throw new InvalidOptions( "ERROR: No output language has been defined" );
	}
		
	if ( !generate_simple && !generate_named )
        {
            generate_simple = true; // if no option defined then we always generate simple classes
        }
        
	if ( options.directories.size() == 0 )
        {
	    options.directories = options.namespaces;
        }
        
	String log = "";
        if ( class_names.size() == 0 )
        {
            if ( generate_simple )
            {
		if ( generate_cpp )
		    log += CodeWriter.write( repository, repository, "C++", directory, options );
		if ( generate_java )
		    log += CodeWriter.write( repository, repository, "Java", directory, options );
	    }
	    
            if ( generate_named )
            {
                options.with_named_template = true;
                if ( generate_cpp )
		    log += CodeWriter.write( repository, repository, "C++", directory, options );
		if ( generate_java )
		    log += CodeWriter.write( repository, repository, "Java", directory, options );
	    }
        }
        else
        {
	    for ( int n = 0; n < class_names.size(); ++n )
            {
		Object cl = repository.getClass( class_names.get( n ) );

		if ( cl == null )
		{
		    log += "ERROR: Class \"" + class_names.get( n ) + "\" is not found in the given XML file.";
		    continue;
		}

		if ( generate_simple )
		{
		    if ( generate_cpp )
			log += CodeWriter.write( repository, cl, "C++", directory, options );
		    if ( generate_java )
			log += CodeWriter.write( repository, cl, "Java", directory, options );
		}

		if ( generate_named )
		{
		    options.with_named_template = true;
		    if ( generate_cpp )
			log += CodeWriter.write( repository, cl, "C++", directory, options );
		    if ( generate_java )
			log += CodeWriter.write( repository, cl, "Java", directory, options );
                    options.with_named_template = false;
		}
	    }
	}
        
	System.err.println( log );	
    }
    
    static private void printUsage()
    {
	System.out.println( "Generator Usage: " );
	System.out.println( "is_generator.sh [options] <XML files>" );
	System.out.println( " [options]     is any combination of the options listed below." );
	System.out.println( " <XML files>   are the names of a files containing XML definitions." );
	System.out.println( "               It is required and must appear last. If it is the only parameter" );
	System.out.println( "               used the IS generator will be started in the interactive mode.\n" );
	System.out.println( "Options:" );
	System.out.println( " -d <dir>" );
	System.out.println( " --outdir <dir>" );
	System.out.println( "        Use <dir> for the output directory instead of the current directory." );
	System.out.println( " -cpp" );
	System.out.println( " --cpp" );
	System.out.println( "        Generate C++ classes." );
	System.out.println( " -java" );
	System.out.println( " --java" );
	System.out.println( "        Generate Java classes." );
	System.out.println( " -h, --help" );
	System.out.println( "        Print this information and exit." );
	System.out.println( " -n, --named" );
	System.out.println( "        Use ISNamedInfo as base class for the generated classes." );
	System.out.println( " -s, --simple" );
	System.out.println( "        Use ISInfo as base class for the generated classes." );
	System.out.println( "        This is the default option." );
	System.out.println( " -p <name>						" );
	System.out.println( " --package <name>" );
	System.out.println( "        The <name> will be used as package name for the generated Java classes" );
	System.out.println( "        and as namespace name for the generated C++ classes." );
	System.out.println( "        In additon <name> will be used as directory name " );
        System.out.println( "        in the include statements of the generated C++ files" );
        System.out.println( "        if the following option ('--prefix') does not set different." );
	System.out.println( " -pd <dir>						" );
	System.out.println( " --prefix <dir>" );
	System.out.println( "        The <dir> will be used as directory name " );
        System.out.println( "        in the include statements of the generated C++ files." );
	System.out.println( " -nn, --no-namespace" );
	System.out.println( "        Namespaces will not be used for C++ classes." );
	System.out.println( " -np, --no-print-function" );
	System.out.println( "        The 'print' function will not be generated for C++ classes." );
	System.out.println( " -cn <name>					" );
	System.out.println( " --class-name <name>" );
	System.out.println( "        Name of the class to be generated. By default all the classes from" );
	System.out.println( "        the current XML file will be generated." );
	System.out.println( " -a, --array" );
	System.out.println( "        The C++ generated classes will use C-like array intead of the std::vector." );
	System.out.println( "        (The generated classes will have private copy operator and copy constructor," );
	System.out.println( "        which will make objects of such classes non copyable.)" );
    }
}


