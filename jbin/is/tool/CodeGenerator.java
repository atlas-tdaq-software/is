package is.tool;

class CodeGenerator
{
    public static void main( String args[] )
    {
        try
        {
            BatchProcessor.process( args );
        }
        catch( BatchProcessor.InvalidOptions ex )
        {
            ex.print();
        }
    }
}

