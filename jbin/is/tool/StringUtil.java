package is.tool;

class StringUtil
{
    public static String Capitalise( String in )
    {
	char[] data = in.toCharArray();
        if ( data.length > 0 )
        {
	    data[0] = Character.toUpperCase( data[0] );
        }
	return String.valueOf( data );
    }
    
    public static String InvertCapitalisation( String in )
    {
	char[] data = in.toCharArray();
        if ( data.length > 0 )
        {
	    char lower = Character.toLowerCase( data[0] );
	    char upper = Character.toUpperCase( data[0] );
            data[0] = ( data[0] == upper ? lower : upper );
        }
	return String.valueOf( data );
    }
}
