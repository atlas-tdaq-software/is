package is.tool;

import java.io.*;
import java.util.*;
import java.text.DateFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

class Repository extends DefaultTreeModel
{    
    private static final String	emptyString = "";
    private static Hashtable<String,Generator>	generators;
    private static Hashtable<String,String> extensions;
    private static char[] invalids = {
	'!', '@', '#', '$', '%', '^', '&', '*', '(', ')', 	
	'-', '+', '=', '|', '\\', '~', '[', ']', '{', '}', 	
	':', ';', '\'', '"', '.', ',', '/', '?', '<', '>', ' '     
    };
            
    static
    {
	generators = new Hashtable<String,Generator>();
	generators.put( "C++", new CppGenerator() );
	generators.put( "Java", new JavaGenerator() );
	
	extensions = new Hashtable<String,String>();
	extensions.put( "C++", ".h" );
	extensions.put( "Java", ".java" );
    }

    class Attribute
    {
	public String	name;
	public String	type;
	public String	description;
	public Boolean	multi_value;
	public Boolean	use_vector;
	public String	init_value;
	
	public Attribute( )
	{
	    name = emptyString;
	    type = emptyString;
	    description = emptyString;
	    init_value = emptyString;
            multi_value = new Boolean( false );
            use_vector = new Boolean( false );
	}
	
	String getDescription( )
	{
	    return description;
	}
	
	String getName( )
	{
	    return replaceInvalidSymbols( name );
	}	
    }
    
    class Class extends DefaultMutableTreeNode
    {
	public String			name;
	public String			superclass;
	public String			description;
	public Vector<Attribute>	attributes;
	    	
	public Class( String name, String description )
	{
	    super( name, false );
	    this.name = name;
	    this.description = description == null ? emptyString : description;
	    attributes = new Vector<Attribute>();
	}
	
	Attribute newAttribute( )
	{
	    return new Attribute();
	}
	
	String getDescription( )
	{
	    return description;
	}
	
	String getName( )
	{
	    return replaceInvalidSymbols( name );
	}
	
	String getClassName( boolean use_named_info )
	{
	    String cname = replaceInvalidSymbols( name );
	    if ( use_named_info )
	    {
	    	cname += "Named" ;
	    }
	    return cname;
	}
	
	String getSuperclassName( boolean use_named_info )
	{
	    String cname = replaceInvalidSymbols( superclass );
	    if ( use_named_info )
	    {
	    	if ( superclass.equals( "Info" ) )
	    	    cname = "NamedInfo";
	    	else
	    	    cname += "Named" ;
	    }
	    return cname;
	}

	String getCode( String language, CodeProducer.Options opts )
	{
	    return getGenerator( language ).produceCode( this, opts );
	}

	public boolean isLeaf()
	{
	    return true;
	}

	public int getChildCount()
	{
	    return 0;
	}
    }

    class File extends DefaultMutableTreeNode
    {
	public Vector<Class>	classes;
	public String		name;
	public String		permissions;
	public String		size;
	public String		modified;
	
	public File( String name )
	{
	    super( name );
	    classes = new Vector<Class>();
	    this.name = name;
	    
	    java.io.File f = new java.io.File( name );
	    permissions = f.canWrite() ? "read,write" : "read only";
	    size = String.valueOf( f.length() );
	    Date d = new Date( f.lastModified() );
	    modified = DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT, Locale.UK ).format( d );
	}
    
	public boolean isLeaf()
	{
	    return false;
	}

	public Class newClass( String name, String description )
	{
	    return new Class( name, description );
	}

	public void addClass( Class cl )
	{
	    classes.add( cl );
	    insertNodeInto( cl, this, classes.size() - 1 );
	}
    }

    Vector<File> files;

    Repository( )
    {
	super( new DefaultMutableTreeNode( "Repository", true ), true );
	files = new Vector<File>();
    }
    
    void load( String filename )
    {
    	File f = new File( filename );
	XMLReader.read( f );
	files.add( f );
	insertNodeInto( f, (DefaultMutableTreeNode)getRoot(), files.size() - 1 );
    }

    void close( File file )
    {
	files.remove( file );
	removeNodeFromParent( (DefaultMutableTreeNode)file );
    }
    
    Class getClass( String name )
    {
	for ( int i = 0; i < files.size() ; i++ )
	{
	    File file = files.get( i );
            for ( int j = 0; j < file.classes.size() ; j++ )
            {
            	Class cl = file.classes.get( j );
                if ( name.equals( cl.name ) )
                    return cl;
            }
	}
	return null;
    }   
    
    static Enumeration getSupportedLanguages( )
    {
    	return generators.keys( );
    }    
    
    static String getFileExtension( String langauge )
    {
    	return (String)extensions.get( langauge );
    }    
    
    static Generator getGenerator( String language )
    {
    	return (Generator)generators.get( language );
    }    
    
    private static String replaceInvalidSymbols( String text )
    {
	for (  int i = 0; i < invalids.length; i++ )
	{
	    text = text.replace( invalids[i], '_' );
	}
	return text;
    }    
}
