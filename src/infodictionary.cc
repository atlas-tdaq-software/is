#include <is/infodictionary.h>
#include <is/server.h>    

void
ISInfoDictionary::checkin(const std::string & name, const ISInfo & info,
    bool keep_history) const
{
    return update(name, info, true, keep_history, false);
}

void
ISInfoDictionary::checkin(const std::string & name, int tag, const ISInfo & info) const
{
    return update(name, info, true, true, true, tag);
}

void
ISInfoDictionary::insert(const std::string & name, int tag, const ISInfo & isi) const
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    is::info info;
    isi.m_name = name;
    isi.m_tag = tag;

    is::odatastream out;
    isi.toWire(oid, info, out);

    IS_INVOKE(create( info ), m_partition, sid, oid)
}

void
ISInfoDictionary::insert(const std::string & name, const ISInfo & isi) const
{
    insert(name, 0, isi);
}

bool
ISInfoDictionary::contains(const std::string & name) const
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    IS_INVOKE_AND_RETURN(contains( oid.c_str() ), m_partition, sid, oid)

    return false;
}

void
ISInfoDictionary::remove(const std::string & name) const
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    IS_INVOKE(remove( oid.c_str() ), m_partition, sid, oid)
}

void
ISInfoDictionary::removeAll(const std::string & server_name, const ISCriteria & criteria) const
{
    IS_INVOKE(remove_all( criteria ), m_partition, server_name, "")
}

void
ISInfoDictionary::findType(const std::string & name, ISType & ist) const
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    is::type_var type;
    IS_INVOKE(get_type( oid.c_str(), type ), m_partition, sid, oid)
    ist = ISType(type);
}

void
ISInfoDictionary::getType(const std::string & name, ISType & ist) const
{
    return findType(name, ist);
}

void
ISInfoDictionary::findValue(const std::string & name, ISInfo & isi) const
{
    is::info_var info;
    is::type type;
    isi.type().type(type);

    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    IS_INVOKE(get_last_value( oid.c_str(), info ), m_partition, sid, oid)

    is::idatastream in(info->value.data);
    isi.fromWire(m_partition, name.c_str(), info->type, info->value, in);
}

void
ISInfoDictionary::findValue(const std::string & name, int tag, ISInfo & isi) const
{
    is::info_var info;
    is::type type;
    isi.type().type(type);

    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    IS_INVOKE(get_value( oid.c_str(), tag, info ), m_partition, sid, oid)

    is::idatastream in(info->value.data);
    isi.fromWire(m_partition, name.c_str(), info->type, info->value, in);
}

void
ISInfoDictionary::getValue(const std::string & name, int tag, ISInfo & isi) const
{
    return findValue(name, tag, isi);
}

void
ISInfoDictionary::getValue(const std::string & name, ISInfo & isi) const
{
    return findValue(name, isi);
}

void
ISInfoDictionary::getTags(const std::string & name,
    std::vector<std::pair<int, OWLTime> > & tags) const
{
    is::tag_list_var list;

    std::string sid, oid;
    is::server::split_name(name, sid, oid);
    IS_INVOKE(get_tags( oid.c_str(), list ), m_partition, sid, oid)
    tags.resize(list->length());
    for (unsigned int i = 0; i < list->length(); i++)
	{
	tags[i] = std::make_pair(list[i].tag,
	    OWLTime(list[i].time / 1000000, list[i].time % 1000000));
    }
}

void
ISInfoDictionary::update(const std::string & name, const ISInfo & isi,
    bool create_if_not_exist, bool keep_history, bool use_tag, int tag) const
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    is::info info;
    isi.m_name = name;
    isi.m_tag = tag;
    is::odatastream out;
    isi.toWire(oid, info, out);

    if (create_if_not_exist)
	IS_INVOKE(checkin( info, keep_history, use_tag ), m_partition, sid, oid)
    else
	IS_INVOKE(update( info, keep_history, use_tag ), m_partition, sid, oid)
}

void
ISInfoDictionary::update(const std::string & name, const ISInfo & isi,
    bool keep_history) const
{
    return update(name, isi, false, keep_history, false);
}

void
ISInfoDictionary::update(const std::string & name, int tag, const ISInfo & isi) const
{
    return update(name, isi, false, true, true, tag);
}

void
ISInfoDictionary::history(const std::string & name, int how_many,
			    is::info_history_var & info, ISInfo::HistorySorted order) const
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);

    IS_INVOKE(get_last_values( oid.c_str(), how_many, order, info ), m_partition, sid, oid)
}
