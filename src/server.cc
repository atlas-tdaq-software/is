#include <string>

#include <ipc/partition.h>

#include <is/server.h>
#include <is/is.hh>

void
is::server::split_name(	const std::string & name,
			std::string & server_name,
			std::string & object_name )
{
    std::string::size_type  pos = name.find( '.' );

    if ( pos == std::string::npos ) {
	throw daq::is::InvalidName( ERS_HERE, name );
    }
        
    server_name = name.substr( 0, pos );
    object_name = name.substr( pos + 1 );
}

is::repository_var
is::server::resolve(	const IPCPartition & partition, 
			const std::string & name )
{
    try {
        return partition.lookup<is::repository>( name );
    }
    catch( daq::ipc::Exception & ex ) {
	throw daq::is::RepositoryNotFound( ERS_HERE, name, ex );
    }
}

bool
is::server::exist(	const IPCPartition & partition, 
			const std::string & name )
{
    try {
    	return partition.isObjectValid<is::repository>( name );
    }
    catch ( ... ) { }
    
    return false;
}
