#include <is/infoany.h>
#include <is/infoiterator.h>

ISInfoIterator::ISInfoIterator( const IPCPartition & partition,
				const std::string & server_name,
				const ISCriteria & criteria )
  : ISInfoStream(partition, server_name, criteria, true, 0),
    m_dictionary(partition)
{
    move(-1);
}

void
ISInfoIterator::value( ISInfo & isi ) const
{
    m_dictionary.getValue(name(), isi);
}

void
ISInfoIterator::value( int tag, ISInfo & isi ) const
{
    m_dictionary.getValue(name(), tag, isi);
}

void
ISInfoIterator::tags( std::vector< std::pair<int,OWLTime> > & tags ) const
{
    m_dictionary.getTags(name(), tags);
}

void
ISInfoIterator::sendCommand( const std::string & command ) const
{
    ISInfoAny info;
    value(info);

    info.sendCommand( command );
}
