#include <ipc/core.h>
#include <ipc/policy.h>

#include <is/datastream.h>
#include <is/info.h>
#include <is/infoany.h>
#include <is/infodynany.h>
#include <is/exceptions.h>

namespace
{
    bool compare(const is::address& a1, const is::address& a2)
    {
    	if( a1.length() != a2.length() )
            return false;
        for ( CORBA::ULong i = 0; i < a1.length(); ++i ) {
            if ( a1[i] != a2[i] )
            	return false;
        }
        return true;
    }
}

ISAnnotation &
ISInfo::getAnnotation(const std::string & name) {
    Annotations::iterator it = m_annotations.find(name);
    if (it != m_annotations.end()) {
        return it->second;
    }
    throw daq::is::AnnotationNotFound(ERS_HERE, name);
}

const ISAnnotation &
ISInfo::getAnnotation(const std::string & name) const {
    Annotations::const_iterator it = m_annotations.find(name);
    if (it != m_annotations.end()) {
        return it->second;
    }
    throw daq::is::AnnotationNotFound(ERS_HERE, name);
}

const ISType &
ISInfo::type() const
{
    if ( m_type.compatibleWith( ISType::Null ) )
    {
	ISInfo * self = const_cast<ISInfo*>( this );
        is::ostream_tie<ISType> out( self ->m_type );
	self -> publishGuts( out );
    }
    return m_type;
}

void
ISInfo::sendCommand( const std::string & command ) const 
{
    try {
	is::provider_var pref = 
        	ipc::use_cache<is::provider,ipc::narrow>::resolve<IPCCore::stringToObject>( 
                		ISInfoProvider::makeConnectString( m_address ) );
	if ( CORBA::is_nil( pref ) )
	    throw daq::is::ProviderNotFound( ERS_HERE, m_name );

	pref -> command ( m_name.c_str(), command.c_str() );
    }
    catch( CORBA::SystemException & ex )
    {
	throw daq::is::ProviderNotFound( ERS_HERE, m_name, 
        	daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    catch( daq::ipc::Exception & ex )
    {
	throw daq::is::ProviderNotFound( ERS_HERE, m_name, ex );
    }
}

bool
ISInfo::providerExist( ) const
{
    if ( compare( m_address, ISInfoProvider::instance().m_address ) ) {
	ERS_DEBUG( 1, "Information address is pointing to the local provider" );
    	return false;
    }
    
    try {
	is::provider_var pref = 
        	ipc::use_cache<is::provider,ipc::non_existent>::resolve<IPCCore::stringToObject>( 
                		ISInfoProvider::makeConnectString( m_address ) );
	return ( !CORBA::is_nil(pref) );
    }
    catch( CORBA::SystemException & ex )
    {
	ERS_DEBUG( 1, ex );
	return false;
    }
    catch( daq::ipc::Exception & ex )
    {
	ERS_DEBUG( 1, ex );
	return false;
    }
}

void 
ISInfo::fromWire(const IPCPartition & partition,
    		const char * name, 
                const is::type & itype, 
                const is::value & value,
                is::idatastream & in)
{
    ISType t(itype);
    if ( m_polymorphic )
    {
	m_type = t;
    }
    else
    {
        if ( !t.subTypeOf( type() ) )
            throw daq::is::InfoNotCompatible( ERS_HERE, name );
    }
    m_partition = partition;
    m_tag = value.attr.tag;
    m_name = name;
    m_address = value.attr.address;
    m_time = OWLTime( value.attr.time/1000000, value.attr.time%1000000 );
    m_annotations.clear();
    for (size_t i = 1; i < value.attr.ann.length(); i += 2) {
        m_annotations.emplace(value.attr.ann[i-1].in(), value.attr.ann[i].in());
    }

    is::istream_tie<is::idatastream> istream( in );
    try {
    	refreshGuts( istream );
    }
    catch( CORBA::MARSHAL & ex ) {
	in.rewind();
	throw daq::is::InfoNotCompatible( ERS_HERE, name, 
        	daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    
    in.rewind();
}

void
ISInfo::toWire(const std::string & object_name, is::info & info, is::odatastream & out) const
{
    m_time = OWLTime(OWLTime::Microseconds);
    m_address = ISInfoProvider::instance().address();

    type().type(info.type);
    info.name = object_name.c_str();
    info.value.attr.tag = m_tag;
    info.value.attr.time = m_time.total_mksec_utc();
    info.value.attr.address = m_address;
    info.value.attr.ann.length(m_annotations.size()*2);
    int i = 0;
    for (auto && a : m_annotations) {
        info.value.attr.ann[i++] = a.first.c_str();
        info.value.attr.ann[i++] = a.second.data().c_str();
    }

    is::ostream_tie<is::odatastream> ostream(out);
    const_cast<ISInfo*>(this)->publishGuts(ostream);
    out.data(info.value.data);
}

std::ostream &
ISInfo::print( std::ostream & out ) const
{
    if ( m_name.size() )
	out << "object '" << m_name 
            << "' of '" << m_type 
            << "' type updated at " << m_time
	    << " with the '" << m_tag << "' tag";
    else
	out << "object of '" << m_type << "' type";
    return out;
}

std::ostream& operator<< ( std::ostream & out, const ISInfo & info )
{
    return info.print( out );
}

std::ostream &
operator<< ( std::ostream& out, ISInfo::Reason reason )
{
    switch ( reason )
    {
	case is::Created:
	    out << "\"InfoCreated\"";
	    break;
	case is::Updated:
	    out << "\"InfoUpdated\"";
	    break;
	case is::Deleted:
	    out << "\"InfoDeleted\"";
	    break;
	case is::Subscribed:
	    out << "\"InfoSubscribed\"";
	    break;
	default:
	    out << "Error:: bad ISInfo::Reason value (" << (int)reason << ")";
	    break;
    }
    return out;
}

