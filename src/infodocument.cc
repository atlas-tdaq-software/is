#include <iostream>

#include <rdb/rdb.hh>

#include <is/infodocument.h>
#include <is/infoany.h>
#include <ipc/exceptions.h>

boost::mutex 						ISInfoDocument::Directory::s_guard;
std::map<IPCPartition,ISInfoDocument::Directory*>	ISInfoDocument::Directory::s_documents;

namespace
{
    const char * const DatabaseName = "ISRepository";

    struct TypeMap : public std::map<std::string,ISType::Basic>
    {
    	TypeMap()
        {
	    (*this)["bool"] = ISType::Boolean;
	    (*this)["s8"] = ISType::S8;
	    (*this)["u8"] = ISType::U8;
	    (*this)["s16"] = ISType::S16;
	    (*this)["u16"] = ISType::U16;
	    (*this)["s32"] = ISType::S32;
	    (*this)["u32"] = ISType::U32;
	    (*this)["s64"] = ISType::S64;
	    (*this)["u64"] = ISType::U64;
	    (*this)["float"] = ISType::Float;
	    (*this)["double"] = ISType::Double;
	    (*this)["string"] = ISType::String;
	    (*this)["date"] = ISType::Date;
	    (*this)["time"] = ISType::Time;
	    (*this)["enum"] = ISType::S32;
	}
        
        ISType::Basic getTypeCode( const std::string & name ) const
        {
            const_iterator it = find( name );
            return ( it != end() ? it->second : ISType::InfoObject );
        }
        
    };
    
    const TypeMap BaseTypesMap;
    
    void replace_carriage_return( std::string & s )
    {
	const std::string c_ret = "\n";
	std::string::size_type pos = 0;
	while( ( pos = s.find( c_ret ) ) != std::string::npos )
	{
	    s.replace( pos, c_ret.size(), "\\n" );
	}
    }
}

ISInfoDocument::Attribute::Attribute( const rdb::RDBAttribute & attribute, size_t position )
  : m_name( attribute.name ),
    m_type( attribute.type ),
    m_range( attribute.range ),
    m_description( attribute.description ),
    m_is_array( attribute.isMultiValue ),
    m_typecode( BaseTypesMap.getTypeCode( m_type ) ),
    m_format( attribute.intFormat == rdb::int_format_oct 
    		? std::oct 
                : attribute.intFormat == rdb::int_format_hex 
                	? std::hex 
                        : std::dec ),
    m_position( position )
{
    replace_carriage_return( m_description );
}

ISInfoDocument::Attribute::Attribute( const rdb::RDBRelationship & relationship, size_t position )
  : m_name( relationship.name ),
    m_type( relationship.classid ),
    m_description( relationship.description ),
    m_is_array( relationship.isMultiValue ),
    m_typecode( ISType::InfoObject ),
    m_format( std::dec ),
    m_position( position )
{
    replace_carriage_return( m_description );
}

ISInfoDocument::Directory::Directory( const IPCPartition & partition )
  : m_partition( partition )
{
    rdb::cursor_var db;
    try {
	db = m_partition.lookup<rdb::cursor>( DatabaseName );
    }
    catch( daq::ipc::Exception & ex ) {
	ERS_DEBUG( 1, "Can not connect to the '" << DatabaseName << "' RDB server: " << ex );
        return ;
    }

    rdb::RDBClassList_var  class_list;
    try {
        db -> get_sub_classes( "Info", class_list );
    }
    catch ( ... ) {
        return ;
    }
   
    for ( unsigned int i = 0; i < class_list -> length(); i++ )
    {
    	try {
            addDocument( class_list[i].name, &class_list[i] );
        }
        catch( daq::is::DocumentNotFound & ex ) {
            ers::debug( ex, 0 );
        }
    }
}

const ISInfoDocument::Holder & 
ISInfoDocument::Directory::addDocument( const char * name, const rdb::RDBClass * cl )
{
    try
    {
	rdb::cursor_var db = m_partition.lookup<rdb::cursor>( DatabaseName );
        
	rdb::RDBClass_var class_holder;
        if ( !cl )
        {
            db -> get_class( name, class_holder );
            cl = &class_holder.in();
        }
        
        ISInfoDocument::Holder doc;
	doc.m_partition = m_partition;
	doc.m_name = name;
	doc.m_description = cl->description;

	rdb::RDBClassList_var  super_classes;
	db -> get_all_super_classes( name, super_classes );

	super_classes->length( super_classes->length() + 1 );
	super_classes[super_classes->length()-1] = *cl;

	int num = 0;
	for ( unsigned int j = 0; j < super_classes -> length(); j++ )
	{
	    rdb::RDBAttributeList_var a_list;
	    db -> get_attributes( super_classes[j].name, a_list );
	    for ( unsigned int k = 0; k < a_list->length(); k++ )
	    {
		if ( a_list[k].isDirect )
		{
		    doc.m_attributes.insert( Attribute( a_list[k], num ) );
		    num++;
		}
	    }

	    rdb::RDBRelationshipList_var r_list;
	    db -> get_relationships( super_classes[j].name, r_list );
	    for ( unsigned int k = 0; k < r_list->length(); k++ )
	    {
		std::string rel_class_name((const char*)r_list[k].classid);
	        if ( r_list[k].isDirect && rel_class_name != doc.m_name )
		{
		    doc.m_attributes.insert( Attribute( r_list[k], num ) );
		    num++;
		}
	    }
	}
	return insert( std::make_pair( name, doc ) ).first->second;
    }
    catch ( CORBA::SystemException & ex )
    {
	throw daq::is::DocumentNotFound( ERS_HERE, name, daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    catch ( rdb::NotFound & ex )
    {
	throw daq::is::DocumentNotFound( ERS_HERE, name );
    }
    catch ( ers::Issue & ex )
    {
	throw daq::is::DocumentNotFound( ERS_HERE, name, ex );
    }
}

ISInfoDocument::Directory &
ISInfoDocument::Directory::getAllDocuments( const IPCPartition & partition )
{   
    boost::mutex::scoped_lock lock( s_guard );

    static Directory * empty_directory =
            new Directory(IPCPartition("this name must not represent a real partition"));
    
    Directory * map;
    std::map<IPCPartition,Directory*>::iterator it = s_documents.find( partition );
    if ( it != s_documents.end() )
    {
	map = it->second;
    }
    else
    {
	map = new Directory( partition );
	if (map->empty()) {
            delete map;
            map = empty_directory;
	} else {
	    s_documents.insert( std::make_pair( partition, map ) );
	}
    }
    return *map;
}

const ISInfoDocument::Holder &
ISInfoDocument::Directory::findDocument( const IPCPartition & partition, 
					 const std::string & name, 
                                         bool lookup_if_not_in_cache )
{       
    ISInfoDocument::Directory & directory = Directory::getAllDocuments( partition );
    
    boost::mutex::scoped_lock lock(directory.m_mutex);
    
    Directory::const_iterator it = directory.find( name );

    if ( it != directory.end() )
    {
	return it->second;
    }
    else if ( lookup_if_not_in_cache )
    {
	return directory.addDocument( name.c_str() );
    }
    else
    {
	throw daq::is::DocumentNotFound( ERS_HERE, name );
    }
}

ISInfoDocument::Iterator::Iterator( const IPCPartition & partition )
  : m_documents( ISInfoDocument::Directory::getAllDocuments( partition ) )
{
    m_it = m_documents.begin();
}
        
bool
ISInfoDocument::Iterator::operator()( )
{
    return ( *this );
}

ISInfoDocument::Iterator::operator bool( )
{
    return ( m_it != m_documents.end() );
}

ISInfoDocument
ISInfoDocument::Iterator::operator*( ) 
{
    if ( m_it == m_documents.end() )
    {
    	throw daq::is::InvalidIterator( ERS_HERE, m_documents.size(), m_documents.size() );
    }
    
    return ISInfoDocument( m_it -> second );
}

ISInfoDocument::Iterator
ISInfoDocument::Iterator::operator++( )
{
    if ( m_it != m_documents.end() )
	++m_it;
    return *this;
}

ISInfoDocument::Iterator
ISInfoDocument::Iterator::operator++( int )
{
    ISInfoDocument::Iterator self = *this;
    if ( m_it != m_documents.end() )
	++m_it;
    return self;
}

ISInfoDocument::Iterator
ISInfoDocument::Iterator::operator--( )
{
    if ( m_it != m_documents.begin() )
	--m_it; 
    else
	m_it = m_documents.end();
    return *this;
}

ISInfoDocument::Iterator
ISInfoDocument::Iterator::operator--( int )
{
    ISInfoDocument::Iterator self = *this;
    if ( m_it != m_documents.begin() )
	--m_it; 
    else
	m_it = m_documents.end();
    return self;
}

size_t
ISInfoDocument::Iterator::size()
{
    return m_documents.size();
}

ISInfoDocument::ISInfoDocument( const IPCPartition & partition, ISInfo & info, bool lookup_if_not_in_cache )
  : m_holder( Directory::findDocument( partition, info.type().name(), lookup_if_not_in_cache ) )
{
    ;
}

ISInfoDocument::ISInfoDocument( const IPCPartition & partition, const ISType & type, bool lookup_if_not_in_cache )
  : m_holder( Directory::findDocument( partition, type.name(), lookup_if_not_in_cache ) )
{
    ;
}

ISInfoDocument::ISInfoDocument( const IPCPartition & partition, const std::string & type, bool lookup_if_not_in_cache )
  : m_holder( Directory::findDocument( partition, type, lookup_if_not_in_cache ) )
{
    ;
}

const ISInfoDocument::Attribute &
ISInfoDocument::attribute ( size_t pos ) const
{
    AttributeVector::iterator it = m_holder.m_attributes.get<1>().find( pos );
    ERS_ASSERT_MSG( it != m_holder.m_attributes.get<1>().end(), "invalid position " << pos << " is given" );
    return *it;
}

const ISInfoDocument::Attribute & 
ISInfoDocument::attribute ( const std::string & name ) const 
{
    AttributeSet::iterator it = m_holder.m_attributes.find( name );
    if ( it == m_holder.m_attributes.end() )
    	throw daq::is::AttributeNotFound( ERS_HERE, name );
    else
    	return *it;
}

size_t
ISInfoDocument::attributePosition( const std::string & name ) const 
{
    AttributeSet::iterator it = m_holder.m_attributes.find( name );
    if ( it == m_holder.m_attributes.end() )
    	throw daq::is::AttributeNotFound( ERS_HERE, name );
    else
    	return it->position();
}
