#include <is/infodynany.h>
#include <is/infoT.h>

ISInfoDynAny::ISInfoDynAny( )
{
    ISInfo::m_polymorphic = true;
}

ISInfoDynAny::ISInfoDynAny( const IPCPartition & partition, const std::string & type_name ) 
  : ISInfo( type_name ),
    m_doc( new ISInfoDocument( partition, type_name, false ) )  
{
    ISInfo::m_polymorphic = true;
    ISInfo::m_partition = partition;
    ISInfo::m_type = ISType( *m_doc );    
    setType( type() );
}

ISInfoDynAny::ISInfoDynAny( const IPCPartition & partition, const ISType & type )
{
    ISInfo::m_polymorphic = true;
    ISInfo::m_partition = partition;
    ISInfo::m_type = type;
    
    setType( type );
}

ISInfoDynAny::ISInfoDynAny( const ISInfoDynAny & ia )
  : ISInfo( ia ),
    m_doc( ia.m_doc )
{
    copyData( ia );
}

ISInfoDynAny & 
ISInfoDynAny::operator=( const ISInfoDynAny & ia )
{
    if ( this != &ia )
    {
	ISInfo::operator=( ia );
        m_doc = ia.m_doc;
	copyData( ia );
    }
    return *this;
}

void
ISInfoDynAny::copyData( const ISInfoDynAny & ia )
{    
    m_data.resize( ia.m_data.size() );
    
    for ( size_t i = 0; i < m_data.size(); ++i )
    {
	m_data[i].reset( ia.m_data[i]->clone() );
    }
}

bool
ISInfoDynAny::isAttributeArray( size_t pos ) const
{
    ERS_ASSERT_MSG( pos < m_data.size(), "invalid position " << pos << " is given" );
    return type().entryArray( pos );
}

bool
ISInfoDynAny::isAttributeArray( const std::string & name ) const 
{
    return isAttributeArray( assure_document( ).attributePosition( name ) );
}

const ISInfoDocument::Attribute & 
ISInfoDynAny::getAttributeDescription( size_t pos ) const 
{
    return assure_document( ).attribute( pos );
}
    
const ISInfoDocument::Attribute & 
ISInfoDynAny::getAttributeDescription( const std::string & name ) const 
{
    return assure_document( ).attribute( name );
}

ISType::Basic
ISInfoDynAny::getAttributeType( size_t pos ) const
{
    ERS_ASSERT_MSG( pos < m_data.size(), "invalid position " << pos << " is given" );
    return type().entryType( pos );
}

ISType::Basic
ISInfoDynAny::getAttributeType( const std::string & name ) const
{
    return getAttributeType( assure_document( ).attributePosition( name ) );
}

size_t
ISInfoDynAny::getAttributesNumber() const
{
    return m_data.size();
}

const ISInfoDocument &
ISInfoDynAny::assure_document( ) const
{
    if ( !m_doc.get() )
	m_doc.reset( new ISInfoDocument( m_partition, type().name(), true ) );
    
    return *m_doc;
}

const ISInfoDocument &
ISInfoDynAny::getDescription(  ) const 
{
    return assure_document( );
}

void
ISInfoDynAny::publishGuts( ISostream & out )
{
    for ( size_t i = 0; i < m_data.size(); ++i )
    {
    	m_data[i]->publishGuts( out );
    }
}

void
ISInfoDynAny::refreshGuts( ISistream & in )
{
    m_doc.reset();
    setType( type() );
    for ( size_t i = 0; i < m_data.size(); ++i )
    {
    	m_data[i]->refreshGuts( in );
    }
}

std::ostream & 
ISInfoDynAny::print( std::ostream & out ) const
{
    ISInfo::print( out );
    out << "[ ";
    for ( size_t i = 0; i < m_data.size(); ++i )
    {
    	m_data[i]->print( out );
	if ( i != m_data.size() - 1 )
            out << ", ";
    }
    out << " ]";
    return out;
}

namespace
{
    template <class T>
    struct MemberCreator
    {
	static ISInfoClonable *
	create( bool is_array, const IPCPartition & , const ISType & )
	{
	    if ( is_array )
		return new ISMemberHolder<std::vector<T> >( );
	    else
		return new ISMemberHolder<T>( );
	}
    };
    
    template <>
    struct MemberCreator<ISInfoDynAny>
    {
	static ISInfoClonable *
	create( bool is_array, const IPCPartition & p, const ISType & t )
	{
	    if ( is_array )
		return new ISMemberHolder<std::vector<ISInfoDynAny>>(
		        std::vector<ISInfoDynAny>( std::allocator<ISInfoDynAny>( p, t ) ) );
	    else
		return new ISMemberHolder<ISInfoDynAny>( ISInfoDynAny( p, t ) );
	}
    };    
}

#define ALLOCATE_ATTRIBUTE(x,y)		case ISType::y : \
						m_data[i].reset(MemberCreator<x>::create(type.entryArray(i),m_partition,type.entryInfoType(i))); \
                                                break;					   
#define ALLOCATE_ATTRIBUTE_SEPARATOR
#define ALLOCATE_ATTRIBUTE_OBJECT_TYPE	ISInfoDynAny

void
ISInfoDynAny::setType( const ISType & type )
{    
    m_data.resize( type.entries() );
    
    for ( size_t i = 0; i < m_data.size(); ++i )
    {
	switch ( type.entryType( i ) )
	{
	    IS_TYPES( ALLOCATE_ATTRIBUTE )
	    default : ERS_ASSERT( false );
	}
    }
}
