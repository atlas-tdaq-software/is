#include <is/type.h>
#include <is/info.h>
#include <is/ostream.h>
#include <is/infodocument.h>

#define IS_TYPE_MAP(_,y)		(*this)[ISType::y] = #y;
#define IS_TYPE_MAP_SEPARATOR

const std::string 	ISType::unknown( "Unknown" );
const std::string 	ISType::null( "Null" );
const std::string 	ISType::error( "Error:: invalid type id" );
const ISType 		ISType::Null( ISType::null );

ISType::Basic2TypeName	ISType::basic_2_type_name;

ISType::ISType( const ISInfoDocument & doc )
  : m_name( doc.name() ),
    m_inverted( false ),
    m_weak( false )
{
    for ( size_t i = 0; i < doc.attributeCount(); ++i ) 
    {
	const ISInfoDocument::Attribute & a = doc.attribute(i);
        unsigned char simple_or_array = a.isArray() ? ArrayMask : 0x0;
        Basic btype = a.typeCode();
        
        m_data += static_cast<char>( static_cast<unsigned char>(btype) | simple_or_array );
        m_id += static_cast<char>( static_cast<unsigned char>(btype) | simple_or_array );
        
        if ( a.typeCode() == ISType::InfoObject )
	{
            ISType t( ISInfoDocument( doc.partition(), a.typeName() ) );
	    m_nested_types.push_back( t );
	    m_id += '{' + t.m_id + '}';
	}
    }
}

ISType::ISType( const is::type & type ) 
  : m_id( (const char*)type.id ),
    m_name( (const char*)type.name ),
    m_inverted( false ),
    m_weak( false )
{
    m_data.assign( (const char *)type.code.get_buffer(), type.code.length() );
    for ( size_t i = 0; i < type.nested_types.length(); i++ )
    	m_nested_types.push_back( ISType(type.nested_types[i]) );
}

void
ISType::operator<<( const ISInfo & info )
{
    operator<< <ISInfo>( info );
    const ISType & type = info.type();
    m_nested_types.push_back( type );
    m_id += '{' + type.m_id + '}';
}

void
ISType::put( const ISInfo * ii, size_t n, size_t s, const ISInfo & o )
{
    put<ISInfo>( ii, n, s, o );
    const ISType & type = o.type();
    m_nested_types.push_back( type );
    m_id += '{' + type.m_id + '}';
}
    
const std::string &
ISType::entryTypeName ( size_t index ) const
{
    size_t info_number = 0;
    for ( size_t i = 0; i < index; i++ )
    	if ( entryType( i ) == ISType::InfoObject ) info_number++;
    if (   entryType( index ) == ISType::InfoObject 
    	&& m_nested_types.size() > info_number )
        return m_nested_types[info_number].m_name;
    else
    	return basicTypeName( entryType( index ) );
}

ISType
ISType::entryInfoType ( size_t index ) const
{
    size_t info_number = 0;
    for ( size_t i = 0; i < index; i++ )
    	if ( entryType( i ) == ISType::InfoObject ) info_number++;
    if (   entryType( index ) == ISType::InfoObject 
    	&& m_nested_types.size() > info_number )
        return m_nested_types[info_number];
    else
    	return ISType();
}

void
ISType::type( is::type & type ) const
{
    type.id = m_id.c_str();
    type.name = m_name.c_str();
    type.code.length( m_data.size() );
    for( size_t i = 0; i < m_data.size(); i++ )
    	type.code[i] = m_data[i];
    type.nested_types.length( m_nested_types.size() );
    for ( size_t i = 0; i < m_nested_types.size(); i++ )
    	m_nested_types[i].type( type.nested_types[i] );
}

std::ostream&
operator<< ( std::ostream& out, const ISType & ist )
{
    out << ist.name();
    return out;
}

std::ostream&
operator<< ( std::ostream & out, const ISType::Basic t )
{
    out << ISType::basicTypeName( t );
    return out;
}

ISType::Basic2TypeName::Basic2TypeName()
{
    IS_TYPES(IS_TYPE_MAP)
}

const std::string &
ISType::basicTypeName( Basic tid )
{
    Basic2TypeName::iterator it = basic_2_type_name.find( tid );
    return ( it == basic_2_type_name.end() ? error : it->second );
}

ISType::Basic
ISType::basicTypeFromName( const std::string & tn )
{
    Basic2TypeName::iterator it = basic_2_type_name.begin( );
    for( ; it != basic_2_type_name.end(); ++it )
    {
    	if ( it->second == tn )
	    return it->first;
    }
    return ISType::Error;
}
