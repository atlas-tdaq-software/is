#include <algorithm>

#include <ers/ers.h>
#include <owl/time.h>

#include <is/server/criteriahelper.h>
#include <is/server/callbackholder.h>
#include <is/server/receiver.h>
#include <is/server/receiverset.h>
#include <is/server/config.h>
#include <is/server/backup.h>

ISCallbackHolder::ISCallbackHolder(
	const is::info & info, 
        const ISReceiverSet & receivers,
	ISReceiverSetLock & receivers_mutex, 
        ISMirror & m)
  : m_name((const char *) info.name),
    m_type(info.type),
    m_mirror(m)
{
    ISReceiverSetLock::scoped_lock lock(receivers_mutex, false); // read-only lock

    for (ISReceiverSet::const_iterator it = receivers.begin(); it != receivers.end();
	++it)
    {
	(*it)->applyCallbacksToInfo(*this);
    }
}

ISCallbackHolder::ISCallbackHolder(const std::string & name, const is::type & type, ISMirror & m)
  : m_name(name), 
    m_type(type),
    m_mirror(m)
{
  ; 
}

void ISCallbackHolder::inform(const is::info & info, is::reason reason)
{
    m_mirror.push(info, reason);

    if (m_callbacks.empty())
    	return;
        
    std::unique_lock<std::mutex> lock(m_callbacks_mutex);
    
    Callbacks::iterator it = m_callbacks.begin();

    while (it != m_callbacks.end())
    {
	if (it->expired())
	{
	    it = m_callbacks.erase(it);
	    ERS_DEBUG( 3, "Expired callback is removed.");
	    continue;
	}
	try
	{
	    boost::shared_ptr<ISCallback> ptr(*it);
	    if (ptr->inform(info, reason))
	    {
		++it;
		ERS_DEBUG( 3,
		    "subscriber for the (" << (const char*)info.name << ") has been notified");
	    }
	    else
	    {
		ERS_DEBUG( 3,
		    "Can not notify subscriber '" << ptr->getID() << "' about changes of the '"
		    << (const char*)info.name << "' info, callback is removed.");

		// release the shared pointer before removing the weak one
		ptr.reset();
		it = m_callbacks.erase(it);
	    }
	}
	catch (boost::bad_weak_ptr & ex)
	{
	    it = m_callbacks.erase(it);
	    ERS_DEBUG( 3, "Expired callback is removed.");
	}
    }
}

void 
ISCallbackHolder::addCallback(const boost::shared_ptr<ISCallback>& c)
{
    std::unique_lock<std::mutex> lock(m_callbacks_mutex);
    m_callbacks.push_back(c);
}

void
ISCallbackHolder::maybeAddCallback(const ISCriteriaHelper& c,
    const boost::shared_ptr<ISCallback>& r)
{
    if (c.match(m_name.c_str(), m_type)) {
	addCallback(r);
    }
}
