#include <ipc/core.h>

#include <is/server/callback.h>
#include <is/server/callbackholder.h>
#include <is/server/receiver.h>

ISReceiver::ISReceiver(const std::string & server_name, is::callback_ptr r)
    : ISInformer(server_name, r)
{
    ;
}

ISReceiver::~ISReceiver()
{
    ERS_DEBUG(1, "'" << key() << "' receiver is destroyed");
}

void
ISReceiver::release()
{
    boost::mutex::scoped_lock lock(m_mutex);
    m_criteria.clear();
    m_names.clear();
}

std::pair<boost::shared_ptr<ISCriteriaHelper>, boost::shared_ptr<ISCallback>>
ISReceiver::addCallback(const is::criteria & c, is::callback_ptr r, int event_mask)
{
    boost::mutex::scoped_lock lock(m_mutex);
    boost::shared_ptr<ISCriteriaHelper> criteria(new ISCriteriaHelper(c));
    boost::shared_ptr<ISCallback> callback(
	ISCallback::create(criteria->toString(), event_mask, r, shared_from_this()));

    // If a new subscription comes then the receiver is alive
    ISInformer::m_failed = false;
    return *m_criteria.insert(m_criteria.end(), std::make_pair(criteria, callback));
}

boost::shared_ptr<ISCallback>
ISReceiver::addCallback(const std::string& name, is::callback_ptr r, int event_mask)
{
    boost::mutex::scoped_lock lock(m_mutex);
    boost::shared_ptr<ISCallback> c(ISCallback::create(name, event_mask, r, shared_from_this()));
    m_names.insert(std::make_pair(name, c));

    // If a new subscription comes then the receiver is alive
    ISInformer::m_failed = false;
    return c;
}

void
ISReceiver::applyCallbacksToInfo(ISCallbackHolder & ch) const
{
    boost::mutex::scoped_lock lock(m_mutex);
    for (auto c : m_criteria)
    {
	ch.maybeAddCallback(*c.first, c.second);
    }

    std::pair<Names::const_iterator, Names::const_iterator> range
	    = m_names.equal_range(ch.stdname());
    for (Names::const_iterator it = range.first; it != range.second; ++it)
    {
	ch.addCallback(it->second);
    }
}

int
ISReceiver::removeCallback(is::callback_ptr r)
{
    boost::mutex::scoped_lock lock(m_mutex);
    Criteria::iterator it = m_criteria.begin();
    for (; it != m_criteria.end(); ++it)
    {
	if (it->second->is_equivalent(r))
	{
	    m_criteria.erase(it);
	    return m_criteria.size() + m_names.size();
	}
    }

    Names::iterator it1 = m_names.begin();
    for (; it1 != m_names.end(); ++it1)
    {
	if (it1->second->is_equivalent(r))
	{
	    m_names.erase(it1);
	    return m_criteria.size() + m_names.size();
	}
    }

    throw is::SubscriptionNotFound();
}

void
ISReceiver::dump(std::ostream & out)
{
    out << getReceiverName() << " {" << std::endl;
    boost::mutex::scoped_lock lock(m_mutex);
    Criteria::iterator it = m_criteria.begin();
    for (; it != m_criteria.end(); ++it)
    {
    	out << "  criteria subscription ";
        it->second->dump(out);
        out << std::endl;
    }

    Names::iterator it1 = m_names.begin();
    for (; it1 != m_names.end(); ++it1)
    {
    	out << "  simple subscription ";
        it1->second->dump(out);
        out << std::endl;
    }
    out << "}";
}
