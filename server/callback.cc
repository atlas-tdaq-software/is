#include <ipc/core.h>

#include <is/server/callback.h>
#include <is/server/receiver.h>

namespace
{
    template<class T>
    struct ISCallbackImpl: public ISCallback
    {
	ISCallbackImpl(const std::string & id, int event_mask,
			typename T::_ptr_type cb, const boost::shared_ptr<ISReceiver> & r)
	    : ISCallback(id, event_mask),
	      m_receiver(r),
	      m_callback(T::_duplicate(cb))
	{ ; }

	bool inform(const is::info & i, is::reason r) const
	{
	    return m_event_mask.test(r) ? m_receiver->inform(m_callback, i, r) : true;
	}

	bool is_equivalent(is::callback_ptr cb) const
	{
	    return cb->_is_equivalent(m_callback);
	}
        
        void dump(std::ostream & out)
        {
            out << "[" << getID() << "]=[" << IPCCore::objectToString(m_callback, IPCCore::Corbaloc) << "]";
        }

    private:
	boost::shared_ptr<ISReceiver>	m_receiver;
	typename T::_var_type		m_callback;
    };
}

ISCallback *
ISCallback::create(const std::string & id, int event_mask, is::callback_ptr callback,
    const boost::shared_ptr<ISReceiver> & r)
{
    is::info_callback_var info_cb;
    try {
	info_cb = is::info_callback::_narrow(callback);

	if (!CORBA::is_nil(info_cb))
	    return new ISCallbackImpl<is::info_callback>(id, event_mask, info_cb, r);
    }
    catch (CORBA::SystemException & ex) {
	ERS_LOG(
	    "Can not cast " << IPCCore::objectToString(callback, IPCCore::Corbaloc) << " callback : " << ex);
    }

    is::event_callback_var event_cb;
    try {
	event_cb = is::event_callback::_narrow(callback);

	if (!CORBA::is_nil(event_cb))
	    return new ISCallbackImpl<is::event_callback>(id, event_mask, event_cb, r);
    }
    catch (CORBA::SystemException & ex) {
	ERS_LOG(
	    "Can not cast " << IPCCore::objectToString(callback, IPCCore::Corbaloc) << " callback : " << ex);
    }

    throw is::InvalidCallback();
}
