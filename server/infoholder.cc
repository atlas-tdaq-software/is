#include <algorithm>

#include <ers/ers.h>
#include <owl/time.h>

#include <is/server/criteriahelper.h>
#include <is/server/infoholder.h>
#include <is/server/receiver.h>
#include <is/server/receiverset.h>
#include <is/server/config.h>
#include <is/server/backup.h>

ISInfoHolder::ISInfoHolder(const is::info & info, const ISReceiverSet & receivers,
			    ISReceiverSetLock & receivers_mutex, ISMirror & m)
    : ISCallbackHolder(info, receivers, receivers_mutex, m)
{
    m_value.insert(info.value);
    inform(info, is::Created);
}

ISInfoHolder::ISInfoHolder(const std::string & name, const is::type & type, ISMirror & m)
    : ISCallbackHolder(name, type, m)
{ ; }

ISInfoHolder::~ISInfoHolder()
{
    try {
        is::info info;
        getLastValue(info);
        inform(info, is::Deleted);
    } catch (is::NotFound & ex) {
        ;
    }

    ERS_DEBUG( 3, "information object '" << m_name << "' is destroyed");
}

is::data *
ISInfoHolder::data() const
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    // return most recent value
    ValuesTimeOrdered::const_reverse_iterator it = m_value.get<1>().rbegin();
    if (it == m_value.get<1>().rend())
    {
	throw is::NotFound();
    }
    return new is::data(it->data);
}

is::data *
ISInfoHolder::data(CORBA::Long tag) const
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    Values::const_iterator it = m_value.find(tag);
    if (it == m_value.end())
    {
	throw is::NotFound();
    }

    return new is::data(it->data);
}

void ISInfoHolder::getLastValue(is::info & info) const
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    ValuesTimeOrdered::const_reverse_iterator it = m_value.get<1>().rbegin();
    if (it == m_value.get<1>().rend())
    {
	throw is::NotFound();
    }

    info.name = m_name.c_str();
    info.type = m_type;
    info.value = *it;
}

void ISInfoHolder::getTagValue(CORBA::Long tag, is::info & info) const
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    Values::const_iterator it = m_value.find(tag);
    if (it == m_value.end())
    {
	throw is::NotFound();
    }

    info.name = m_name.c_str();
    info.type = m_type;
    info.value = *it;
}

void ISInfoHolder::update(const is::info & info, bool keep_history, bool use_tag)
{
    if (!is::types_equal(m_type, info.type))
	throw is::NotCompatible();

    {
	std::unique_lock<std::mutex> lock(m_data_mutex);

	if (use_tag)
	{
	    Values::const_iterator it = m_value.find(info.value.attr.tag);
	    if (it != m_value.end()) {
                m_value.modify(it, [&info](is::value & v) {
                        v.attr = info.value.attr;
                        v.data = info.value.data;
                });
	    }
	    else {
		m_value.insert(info.value);
	    }
	}
	else
	{
	    if (keep_history || ISConfig::ForcedHistory)
	    {
		Values::const_reverse_iterator it = m_value.rbegin();
		const_cast<is::value &>(info.value).attr.tag = it->attr.tag + 1;
		m_value.insert(info.value);
	    }
	    else {
	        ValuesTimeOrdered::const_iterator latest = std::prev(m_value.get<1>().end());
		m_value.modify(m_value.project<0>(latest), [&info](is::value & v) {
			v.attr = info.value.attr;
			v.data = info.value.data;
		});
	    }
	}

        ValuesTimeOrdered::iterator oldest = m_value.get<1>().begin();
	if (m_value.size() > ISConfig::HistoryNumDepth) {
	    m_value.get<1>().erase(oldest++);
	}

	if ((info.value.attr.time - oldest->attr.time) > ISConfig::HistoryTimeDepth)
	{
	    m_value.get<1>().erase(oldest,
		m_value.get<1>().upper_bound(info.value.attr.time - ISConfig::HistoryTimeDepth));
	}
    }
    
    inform(info, is::Updated);
}

void ISInfoHolder::getTags(is::tag_list_out & list) const
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    list = new is::tag_list(m_value.size());
    list->length(m_value.size());

    CORBA::ULong i = 0;
    for (ValuesTimeOrdered::const_reverse_iterator it = m_value.get<1>().rbegin();
	it != m_value.get<1>().rend(); ++it, ++i)
    {
	list[i].tag = it->attr.tag;
	list[i].time = it->attr.time;
    }
}

void ISInfoHolder::addCallback(const boost::shared_ptr<ISCallback>& c)
{
    ISCallbackHolder::addCallback(c);
    if (c->eventMask().test(is::Subscribed)) {
	is::info info;
	getLastValue(info);
	c->inform(info, is::Subscribed);
    }
}

void ISInfoHolder::write(std::ostream & out) const
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    unsigned int length = m_value.size();
    out.write((const char *) &length, sizeof(length));

    for (Values::const_iterator it = m_value.begin(); it != m_value.end(); ++it) {
	out <<= *it;
    }
}

void ISInfoHolder::read(std::istream & in)
{
    std::unique_lock<std::mutex> lock(m_data_mutex);

    unsigned int length = 0;
    in.read((char *) &length, sizeof(length));

    is::info info;
    info.name = m_name.c_str();
    info.type = m_type;

    for (unsigned int i = 0; i < length; i++)
    {
	is::value v;
	in >>= v;
	m_value.insert(v);
	info.value = v;
	inform(info, is::Created);
    }
}

