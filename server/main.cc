////////////////////////////////////////////////////////////////////////
//    main.cc
//
//    IS server main function
//
//    Sergei Kolos,    January 2000
//
//    description:
//	Implements server functionality for the Information Service
//		
////////////////////////////////////////////////////////////////////////

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>

#include <cmdl/cmdargs.h>

#include <omniORB4/omniInterceptors.h>

#include <ipc/signal.h>

#include <is/server/replica.h>
#include <is/server/config.h>
#include <is/server.h>

#include <pmg/pmg_initSync.h>

ERS_DECLARE_ISSUE( is,
    CantCreateDirectory,
    "Backup directory '" << directory << "' can not be created",
    ((std::string)directory )
    )

namespace
{
    std::list<std::pair<std::string, std::string> >
    get_default_options(int & argc, char ** & argv)
    {
	std::list<std::pair<std::string, std::string> > options = IPCCore::extractOptions(
	    argc, argv);
	options.push_front(
	    std::make_pair(std::string("clientCallTimeOutPeriod"), std::string("1000")));
	options.push_front(
	    std::make_pair(std::string("scanGranularity"), std::string("30")));
	options.push_front(
	    std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
	options.push_front(
	    std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));

	return options;
    }
}

int main(int argc, char ** argv)
{
    std::list<std::pair<std::string, std::string> > options;

    try
    {
	options = get_default_options(argc, argv);
    }
    catch (daq::ipc::Exception & ex)
    {
	ers::fatal(ex);
	return 1;
    }

    CmdArgInt period('t', "time", "seconds",
	"delay between backup operations (0 (default) disables periodic backup, "
	"-1 disables the final one as well)");
    CmdArgStr partition_name('p', "partition", "name", "partition to work in.");
    CmdArgStr server_name('n', "server", "name", "server to start.",
	CmdArg::isREQ);
    CmdArgStr restore_from('r', "restore", "file",
	"backup file to restore from.");
    CmdArgStr backup_to('b', "backup", "file", "file to backup to.");
    CmdArgInt history_number('N', "history-depth", "number",
	"set maximum number of values for the same information object (-1 means infinite number).");
    CmdArgInt history_time('D', "history-time-depth", "seconds",
	"set maximum time range for values of the same information object (-1 (default) means infinite range).");
    CmdArgInt workers('w', "workers", "number", "number of worker threads.");
    CmdArgBool pmg_sync('s', "sync", "use PMG synchronization.");
    CmdArgBool forced_history('H', "history", "keep history for all information objects.");

    CmdLine cmd(*argv, &period, &partition_name, &server_name, &restore_from,
	&backup_to, &history_number, &history_time, &pmg_sync, &workers, &forced_history, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    cmd.description(
	"This program implements server functionality for the Information Service.\n");

    period = 0;
    history_number = ISConfig::HistoryNumDepth;
    history_time = -1;
    workers = ISConfig::WorkerThreads;
    forced_history = ISConfig::ForcedHistory;

    // Parse arguments

    cmd.parse(arg_iter);

    if ((unsigned int)history_number != ISConfig::HistoryNumDepth) {
	ISConfig::HistoryNumDepth = history_number;
    }

    if (history_time != -1) {
	ISConfig::HistoryTimeDepth = (int64_t)history_time * 1000000;
    }

    if (forced_history != ISConfig::ForcedHistory) {
	ISConfig::ForcedHistory = forced_history;
    }

    if (workers < 1) {
	ERS_LOG( "Invalid number of worker threads value : " << workers
	    << " was given. The default value will be used.");
    }
    else {
	ISConfig::WorkerThreads = workers;
	ISConfig::MaxConcurrentCallbacks = workers-1;
	ERS_LOG( "number of workers is set to " << ISConfig::WorkerThreads);
	ERS_LOG(
	    "number of concurrent callbacks is set to " << ISConfig::MaxConcurrentCallbacks);

	// +2 for the omniORB service threads
	options.push_front(std::make_pair(std::string("maxServerThreadPoolSize"),
	    boost::lexical_cast<std::string>((int) workers + 2)));

	// +1 for the delayed callbacks queue thread
	options.push_front(std::make_pair(std::string("maxGIOPConnectionPerServer"),
	    boost::lexical_cast<std::string>((int) workers + 1)));
    }

    try {
	IPCCore::init(options);
    }
    catch (daq::ipc::Exception & ex) {
	ers::fatal(ex);
	return 1;
    }

    IPCPartition partition(partition_name);
    std::string server(server_name);
    std::string restore_file(
	restore_from.flags() && CmdArg::GIVEN ? (const char *) restore_from : "");
    std::string backup_file(
	backup_to.flags() && CmdArg::GIVEN ? (const char *) backup_to : "");

    if (!backup_file.empty())
    {
	boost::filesystem::path path(boost::filesystem::path(backup_file).parent_path());
	try
	{
	    if (!exists(path))
		boost::filesystem::create_directories(path);
	}
	catch (std::exception & ex)
	{
	    ers::fatal(is::CantCreateDirectory(ERS_HERE, path.string(), ex ) );
	    return 1;
	}
    }

    if (server.find('.') != std::string::npos)
    {
	ers::fatal(daq::is::InvalidServerName(ERS_HERE, server, partition.name() ) );
	return 2;
    }

    if (is::server::exist(partition, server))
    {
	ers::fatal(daq::is::ServerAlreadyExist(ERS_HERE, server, partition.name() ) );
	return 3;
    }

    try
    {
        ISReplica r(partition, server, restore_file, backup_file, period);

	if (pmg_sync.flags() && CmdArg::GIVEN)
	{
	    pmg_initSync();
	}

	ERS_LOG(
	    "the '" << server << "' IS server started in the partition '" << partition.name() << "'");

        int sig = daq::ipc::signal::wait_for();
        ERS_LOG("Signal " << sig << " received, stopping '" << server << "' IS server ... ");
    }
    catch (ers::Issue & ex)
    {
	ers::fatal(ex);
    }

    ERS_LOG( "done");

    return 0;
}
