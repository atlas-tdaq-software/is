#!/bin/sh
#################################################################
#
#	Test Script for the IS package
#	Created by Sergei Kolos; 10.02.99
#
#################################################################

start_partition_command=`echo $0 | sed -e 's/\/run.sh/\/start_partition.sh/g'`
$start_partition_command

is_server_command="$1is_server -n IStest -t 5 "
is_source_command="$2is_f_source -n IStest -v -w 30 -N 1 2 3 "
is_named_info_test="$2is_named_info -n IStest  -N 1 2 3 "
is_receiver_command="$2is_f_receiver -n IStest -v -N 1 2 3"
is_commander_test="$2is_f_commander -n IStest -v -N 1 2 3 "
rdb_server_command="rdb_server -d ISRepository -S $3data/is.xml $3data/test.xml $3data/example.xml"

deactivate_receiver()
{
    killall is_f_receiver
}

echo "####################################################"
echo "IS test started at `date`"
echo "####################################################"

#############################################
# Start server
#############################################
start_server()
{
    output_file=$1.out
    error_file=$1.err
    rm -f $output_file
    rm -f $error_file
    echo "--------------------------------------------------------------------------------------------------------------"
    echo -n "Executing \"$2\" command ... "

    $2 > $output_file 2> $error_file &
    
    count=0
    while test ! -s $output_file -a ! -s $error_file -a $count -lt 30
    do
	sleep 1
	count=`expr $count + 1`
    done

    if test -s $error_file
    then
	Fatal=`grep "FATAL" $error_file`
	if test -n "$Fatal"
	then
	    echo "FAILED"
	    cat $error_file
	    exit 1
	fi
    fi
    if test -s $output_file
    then
    	echo "Ok"
    else
	echo "FAILED (Timeout)"
    fi
    echo "--------------------------------------------------------------------------------------------------------------"
}

#############################################
# Start IS server
#############################################
start_server "IS_SERVER" "$is_server_command"

#############################################
# Start RDB server
#############################################
start_server "RDB_SERVER" "$rdb_server_command"

#############################################
# Start receiver
#############################################
start_server "IS_RECEIVER" "$is_receiver_command"

#############################################
# Start clients
#############################################

echo "Testing ISNamedInfo ... "
$is_named_info_test || { echo "ERROR: IS check failed 1" ; exit 1 ; }
echo "done."
echo "Testing ISInfo ... "
$is_source_command || { echo "ERROR: IS check failed 2" ; exit 1 ; } &
sleep 5
echo "Starting commander ... "
$is_commander_test || { echo "ERROR: IS check failed 3" ; exit 1 ; }
echo "done."

#############################################
# Wait for server deactivation
#############################################
echo "deactivating receiver... \c"
sleep 3
deactivate_receiver
